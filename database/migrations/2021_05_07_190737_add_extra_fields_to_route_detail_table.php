<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToRouteDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_detail', function (Blueprint $table) {
            $table->integer('id_client');
            $table->integer('id_location');
            $table->integer('packages');
            $table->string('evidence')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('route_detail', function (Blueprint $table) {
            $table->dropColumn('id_client');
            $table->dropColumn('id_location');
            $table->dropColumn('packages');
            $table->dropColumn('evidence');
        });
    }
}
