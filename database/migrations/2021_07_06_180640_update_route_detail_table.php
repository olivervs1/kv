<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRouteDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_detail', function (Blueprint $table) {
            $table->string('receiver')->nullable();
            $table->string('sign')->nullable();
            $table->text('survey')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('route_detail', function (Blueprint $table) {
            $table->dropColumn('receiver');
            $table->dropColumn('sign');
            $table->dropColumn('survey');
        });
    }
}
