<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title></title>

        <style type="text/css" media="screen">
            div.content {
                max-width: 600px;
                margin: 0 auto;
                display: block;
            }

            div.header, div.title, p {
                margin-bottom: 20px;
            }

            div.header {
                text-align: center;
                width: 100%;
                display: block;
                margin: 0 auto;
            }

            div.header img.logo {
                width: auto;
                max-height: 70px;
                text-align: right;
                padding: 20px;
                margin: 0 auto;
            }

            div.title {
                font-size: 30px;
                width: 100%;
                margin: 0 auto;
                padding-top: 20px;
                margin-bottom: 10px;
            }

            div.confidential {
                margin-bottom: 30px;
                width: 100%;
                margin: 0 auto;
            }

            p {
                width: 100%;
                display: block;
            }

            div.confidential p {
                text-align: justify;
                font-size: 10px;
                color: gray;
                margin-bottom: 10px;
            }

            p.content-text {
                font-size: 20px;
                padding: 10px 0px;
                margin: 0 auto; 
            }

            p.firm {
                font-size: 20px;
                text-align: left;
                padding: 10px 0px;
                margin-bottom: 10px;
            }

            p.subtitle {

                font-size: 24px;

                padding: 10px 0px;

            }

            table {
                width: 100%;
                max-width: 600px;
                margin: 10px 0px;
            }

            .text-right {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="header">
                
            </div>

            <div class="title">
                La IP del servidor ha cambiado
            </div>

            <p class="content-text text-center">
                {{ $ip }}
            </p>

            <p class="firm">
                KV Transportes
            </p>

            <div class="confidential">
                <p>
                    Este correo electrónico es confidencial y/o puede contener información privilegiada.

                    Si usted no es su destinatario o no es alguna persona autorizada por éste para recibir sus correos electrónicos, NO deberá usted utilizar, copiar, revelar, o tomar ninguna acción basada en este correo electrónico o cualquier otra información incluida en él, favor de notificar al remitente de inmediato mediante el reenvío de este correo electrónico y borrar a continuación totalmente este correo electrónico y sus anexos.

                    Nota: Los acentos y caracteres especiales fueron omitidos para su correcta lectura en cualquier medio electrónico.
                </p>

                <p>
                    This e-mail is confidential and/or may contain privileged information.

                    If you are not the addressee or authorized to receive this for the addressee, you must not use, copy, disclose, or take any action based on this message or any other information herein, please advise the sender immediately by reply this e-mail and delete this e-mail and its attachments.
                </p>
            </div>
        </div>
    </body>
</html>