<aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/home') }}" class="brand-link navbar-dark">
      <img src="{{asset('img/Logo_45.jpg')}}" alt="N131" class="brand-image elevation-3"
           style="opacity: 1">
      <span class="brand-text font-weight-light"></span>
    </a>
      <!-- Sidebar -->
      <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
           <a href="{{ url('/home') }}" class="nav-link">
             <i class="fas fa-tachometer-alt nav-icon"></i>
             <p>
               Dashboard
             </p>
           </a>
          </li>
          <li class="nav-header">Clientes</li>
          <li class="nav-item">
            <a href="{{ url('/clientes') }}" class="nav-link">
              <i class="fas fa-users nav-icon"></i>
              <p>
                Clientes
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/ubicaciones') }}" class="nav-link">
              <i class="fas fa-map-marked-alt nav-icon"></i>
              <p>
                Ubicaciones
              </p>
            </a>
          </li>
          <li class="nav-header">KV</li>
          <li class="nav-item">
            <a href="{{ url('/rutas') }}" class="nav-link">
              <i class="fas fa-road nav-icon"></i>
              <p>
                Rutas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/rutas-preconfiguradas') }}" class="nav-link">
              <i class="fas fa-road nav-icon"></i>
              <p>
                Rutas preconfiguradas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/choferes') }}" class="nav-link">
              <i class="fas fa-address-card nav-icon"></i>
              <p>
                Choferes
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/vehiculos') }}" class="nav-link">
              <i class="fas fa-truck nav-icon"></i>
              <p>
                Vehiculos
              </p>
            </a>
          </li>
          <li class="nav-header">Catálogos</li>
          <li class="nav-item">
            <a href="{{ url('/encuesta') }}" class="nav-link">
              <i class="fas fa-list-ol nav-icon"></i>
              <p>
                Encuesta
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/motivos-de-cancelacion') }}" class="nav-link">
              <i class="fas fa-times nav-icon"></i>
              <p>
                Motivos de cancelación
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/estatus-rutas') }}" class="nav-link">
              <i class="fas fa-file-invoice-dollar nav-icon"></i>
              <p>
                Estatus de rutas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/tipos-de-rutas') }}" class="nav-link">
              <i class="fas fa-road nav-icon"></i>
              <p>
                Tipos de rutas
              </p>
            </a>
          </li>
          <li class="nav-header">Configuración</li>
          <li class="nav-item">
            <a href="{{ url('/gameplanet') }}" class="nav-link">
              <i class="fas fa-user-friends nav-icon"></i>
              <p>
                Gameplanet
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/carta-porte') }}" class="nav-link">
              <i class="fas fa-archive nav-icon"></i>
              <p>
                Archivo Cartas Porte
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/usuarios') }}" class="nav-link">
              <i class="fas fa-user-friends nav-icon"></i>
              <p>
                Usuarios
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/roles') }}" class="nav-link">
              <i class="fas fa-user-friends nav-icon"></i>
              <p>
                Roles
              </p>
            </a>
          </li>
      </ul>
    </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>