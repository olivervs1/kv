<!DOCTYPE html>
<html>
@include('template.head')
@yield('stylesheets')
<body class="hold-transition sidebar-mini layout-fixed layout-footer-fixed layout-navbar-fixed text-sm">
<div class="wrapper">

 @include('template.header') 
 @include('template.sidebar')
 @yield('content')
 @include('template.footer')
</div>
 @include('template.scripts')
 @yield('scripts')
</body>
</html>
