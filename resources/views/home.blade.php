@extends('template.main')

@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <div class="row col-12">                      
                        <div class="col-md-6">
                            <h1 class="m-0 text-dark">Dashboard</h1>
                        </div>
                        <div class="col-md-6 row justify-content-end">
                            <input id="dashboard_date" type="date" value="{{ $date }}" class="form-control col-4"/>
                            <button id="change_date" class="btn btn-primary col-3 ml-2">
                                Buscar
                                <i class="fas fa-search ml-2"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
                      <div class="card-body">
                        <h3 class="card-title">Rutas</h3>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Chofer</th>
                                    <th>Cliente - Ubicación</th>
                                    <th>Hora de incio</th>
                                    <th>Hora de Finalización</th>
                                    <th>Paquetes</th>
                                    <th>Encuesta</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($routes as $r)
                                    @foreach($r->Detail as $rd)
                                        <tr>
                                            <td class="align-middle">{{ $r->Driver->name }}</td>
                                            <td class="align-middle">{{ $rd->Client->comercial_name . ' - ' . $rd->Location->name }}</td>
                                            <td class="align-middle text-center">{{ $rd->start }}</td>
                                            <td class="align-middle text-center">{{ $rd->end }}</td>
                                            <td class="align-middle text-center">{{ $rd->packages }}</td>
                                            <td class="align-middle">
                                                @if($rd->survey != NULL)
                                                  <ul>
                                                    @foreach(json_decode($rd->survey) as $s)
                                                      @php($question = App\Models\Survey::find($s->id))

                                                      @if($question instanceof App\Models\Survey)
                                                        <li>{{ $question->question }}: {{ $s->score }} <i class="fas fa-star text-warning"></i></li>
                                                      @endif
                                                    @endforeach
                                                  </ul>
                                                @endif
                                            </td>
                                            <td class="align-middle text-center">
                                                {{ $rd->Status->name }}
                                                <i class="fas fa-circle" style="color: {{ $rd->Status->color  }}"></i>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/dashboard.js') }}"></script>
@endsection