@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('clientes')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($client)
                <h1 class="m-0 text-dark">Editar cliente</h1>
              @else
                <h1 class="m-0 text-dark">Agregar cliente</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($client)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($client)
                <form id="form" method="POST" action="{{url('/clientes/'. $client->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/clientes')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="company_name">Razón social</label>
                      <input type="text" class="form-control required" id="company_name" name="company_name" placeholder="Razón social" value="{{ isset($client) ? $client->company_name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="comercial_name">Nombre comercial</label>
                      <input type="text" class="form-control" id="comercial_name" name="comercial_name" placeholder="Nombre comercial" value="{{ isset($client) ? $client->comercial_name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="rfc">RFC</label>
                      <input type="text" class="form-control" id="rfc" name="rfc" placeholder="RFC" value="{{ isset($client) ? $client->rfc : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="contact_name">Nombre de contacto</label>
                      <input type="text" class="form-control required" id="contact_name" name="contact_name" placeholder="Nombre de contacto" value="{{ isset($client) ? $client->contact_name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="contact_phone">Teléfono de contacto</label>
                      <input type="text" class="form-control required" id="contact_phone" name="contact_phone" placeholder="Teléfono de contacto" value="{{ isset($client) ? $client->contact_phone : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="contact_email">Correo de contacto</label>
                      <input type="text" class="form-control required" id="contact_email" name="contact_email" placeholder="Correo de contacto" value="{{ isset($client) ? $client->contact_email : ''}}">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection