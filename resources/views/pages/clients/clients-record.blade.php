@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('clientes')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Cliente</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
                <a href="{{ url('/clientes/' . $client->id . '/editar') }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Razón social</span>
                    <span>{{ $client->company_name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre comercial</span>
                    <span>{{ $client->comercial_name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">RFC</span>
                    <span>{{ $client->rfc }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre de contacto</span>
                    <span>{{ $client->contact_name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Teléfono de contacto</span>
                    <span>{{ $client->contact_phone }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Correo de contacto</span>
                    <span>{{ $client->contact_email }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h5>Ubicaciones</h5>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Nombre de ubicación</th>
                    <th>Dirección</th>
                    <th>Estado</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($client->Locations) > 0)
                    @foreach($client->Locations as $l)
                      <tr>
                        <td>{{ $l->name }}</td>
                        <td>
                          {{ $l->street . ', # exterior ' . $l->ext_number }}
                            @if($l->int_number != "") 
                              {{ ', #interior ' . $l->int_number }}
                            @endif
                          {{ ', ' . $l->colony, ', ' . $l->city . ', ' . $l->state . ', ' . $l->zipcode }}
                        </td>
                        <td>{{ $l->state }}</td>
                      </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="3" class="text-center">Sin ubicaciones aún</td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
@endsection