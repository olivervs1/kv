@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('clientes/agregar')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Clientes</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="shows" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Razón social</th>
                    <th>Nombre comercial</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($clients as $c)
                  <tr>
                    <td>
                      <a href="{{url('clientes/'.$c->id)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('clientes/'.$c->id.'/editar')}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('clientes/'.$c->id.'/eliminar')}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{$c->company_name}}</td>
                    <td>{{$c->comercial_name}}</td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#shows').DataTable({
      responsive: true
    });
    $('#shows').show();
    $(window).trigger('resize');
} );
</script>

@endsection