@extends('template.main')
@section('stylesheets')
  <!-- CSS DE OPENLAYERS -->
  <link href="{{ asset('css/ol.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('ubicaciones')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($location)
                <h1 class="m-0 text-dark">Editar ubicación</h1>
              @else
                <h1 class="m-0 text-dark">Agregar ubicación</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($location)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($location)
                <form id="form" method="POST" action="{{url('/ubicaciones/'. $location->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/ubicaciones')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="id_client">Cliente</label>
                      <select class="form-control" id="id_client" name="id_client">
                        @foreach($clients as $c)
                          <option value="{{ $c->id }}" {{ isset($location) ? $location->id_client == $c->id ? "selected" : "" : "" }}>{{ $c->comercial_name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-6"></div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre de ubicación</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de ubicación" value="{{ isset($location) ? $location->name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="store_id">ID de tienda</label>
                      <input type="text" class="form-control" id="store_id" name="store_id" placeholder="ID de Tienda" value="{{ isset($location) ? $location->store_id : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="store_key">CLAVE</label>
                      <input type="text" class="form-control" id="store_key" name="store_key" placeholder="CLAVE de Tienda" value="{{ isset($location) ? $location->store_key : ''}}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="zipcode">Código postal</label>
                      <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Código Postal" value="{{ isset($location) ? $location->zipcode : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="colony">Colonia</label>
                      <input type="text" class="form-control required" id="colony" name="colony" placeholder="Colonia" value="{{ isset($location) ? $location->colony : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="street">Calle</label>
                      <input type="text" class="form-control required" id="street" name="street" placeholder="Calle" value="{{ isset($location) ? $location->street : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="ext_number"># Exterior</label>
                      <input type="text" class="form-control required" id="ext_number" name="ext_number" placeholder="# Exterior" value="{{ isset($location) ? $location->ext_number : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="int_number"># Interior</label>
                      <input type="text" class="form-control" id="int_number" name="int_number" placeholder="# Interior" value="{{ isset($location) ? $location->int_number : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="city">Delegación o municipio</label>
                      <input type="text" class="form-control required" id="city" name="city" placeholder="Delegación o municipio" value="{{ isset($location) ? $location->city : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="state">Estado</label>
                      <input type="text" class="form-control required" id="state" name="state" placeholder="Estado" value="{{ isset($location) ? $location->state : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="latitude">Latitud</label>
                      <input type="text" class="form-control" id="latitude" name="latitude" placeholder="Latitud" value="{{ isset($location) ? $location->lat : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="longitude">Longitud</label>
                      <input type="text" class="form-control" id="longitude" name="longitude" placeholder="Longitud" value="{{ isset($location) ? $location->long : ''}}">
                    </div>
                  </div>
                </div>
                <div id="map" style="height : 400px" class="w-100"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<!-- JS DE OPENLAYERS -->
<script src="{{ asset('js/ol.js') }}"></script>
<script src="{{ asset('js/ol-ext.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>

<script type="text/javascript">
$(document).ready(function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  } else {
    alert('La geolocalización no esta activada en este navegador.');
  }   
});

function successFunction(position) {
    var lat = position.coords.latitude;
    @isset($location)
      lat = {{ $location->lat }}
    @endisset
    var lng = position.coords.longitude;
    @isset($location)
      lng = {{ $location->long }}
    @endisset

    initMap(lng, lat);
}

function errorFunction(position) {
    alert('Error!');
}

var initMap = function(lng, lat) {
  var map = new ol.Map({
   target: 'map',
   layers: [
     new ol.layer.Tile({
       source: new ol.source.OSM()
     }) 
   ],
   view: new ol.View({
     center: ol.proj.toLonLat([lat, lng]),
     zoom: 18,
     projection: 'EPSG:4326'
   })
 });

// Current selection
var sLayer = new ol.layer.Vector({
  source: new ol.source.Vector(),
  style: new ol.style.Style({
    image: new ol.style.Circle({
      radius: 5,
      stroke: new ol.style.Stroke ({
        color: 'rgb(255,165,0)',
        width: 3
      }),
      fill: new ol.style.Fill({
        color: 'rgba(255,165,0,.3)'
      })
    }),
    stroke: new ol.style.Stroke ({
      color: 'rgb(255,165,0)',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255,165,0,.3)'
    })
  })
});
map.addLayer(sLayer);

var search = new ol.control.SearchNominatim ({ //target: $(".options").get(0),
      polygon: $("#polygon").prop("checked"),
      reverse: false,
      position: true  // Search, with priority to geo position
    });
map.addControl (search);

// Select feature when click on the reference index
search.on('select', function(e) { // console.log(e);
    sLayer.getSource().clear();
    // Check if we get a geojson to describe the search
    if (e.search.geojson) {
      var format = new ol.format.GeoJSON();
      var f = format.readFeature(e.search.geojson, { dataProjection: "EPSG:4326", featureProjection: map.getView().getProjection() });
      sLayer.getSource().addFeature(f);
      var view = map.getView();
      var resolution = view.getResolutionForExtent(f.getGeometry().getExtent(), map.getSize());
      var zoom = view.getZoomForResolution(resolution);
      var center = ol.extent.getCenter(f.getGeometry().getExtent());
      // redraw before zoom
      setTimeout(function(){
          view.animate({
          center: center,
          zoom: Math.min (zoom, 16)
        });
      }, 100);
    }
    else {
      $('input#latitude').val(e.coordinate[0]);
      $('input#longitude').val(e.coordinate[1]);

      map.getView().animate({
        center:e.coordinate,
        zoom: Math.max (map.getView().getZoom(),16)
      });
    }
  });

var style = 
  [ new ol.style.Style(
      { image: new ol.style.Shadow(
        { radius: 15,
        }),
        stroke: new ol.style.Stroke(
        { color: [0,0,0,0.3],
          width: 2
        }),
        fill: new ol.style.Fill(
          { color: [0,0,0,0.3]
          }),
        zIndex: -1
      }),
    new ol.style.Style(
      { /* image: new ol.style.Icon({ src:"data/camera.png", scale: 0.8 }), */
        image: new ol.style.RegularShape(
        { radius: 10,
          radius2: 5,
          points: 5,
          fill: new ol.style.Fill({ color: 'blue' })
        }),
      stroke: new ol.style.Stroke(
        { color: [0,0,255],
          width: 2
        }),
      fill: new ol.style.Fill(
        { color: [0,0,255,0.3]
        })
      })
  ];
  style[1].getImage().getAnchor()[1] += 10;

  // Vector layer
  var source = new ol.source.Vector();
  var vector = new ol.layer.Vector(
  { source: source,
    style: style
  });
  map.addLayer(vector);

  map.addInteraction(new ol.interaction.Select());

  /* Use filter or opacity * /
    var c = map.getView().getCenter()
    var g = ol.geom.Polygon.fromExtent(ol.extent.buffer(ol.extent.boundingExtent([c]),500000));
    vector.addFilter(new ol.filter.Crop({
      feature: new ol.Feature(g),
      fill: new ol.style.Fill({color: [255,0,0,.5]})
    }));
    vector.setOpacity(.5)
    /**/

    // Add a feature on the map
    function addFeatureAt(p)
    { var f, r = map.getView().getResolution() *10;
      switch ($("#geom").val())
      { case 'LineString':
          f = new ol.Feature(new ol.geom.LineString(
            [ [p[0]-8*r,p[1]-3*r], 
              [p[0]-2*r,p[1]+1*r], 
              [p[0]+2*r,p[1]-1*r], 
              [p[0]+8*r,p[1]+3*r]
            ]));
          break;
        case 'Polygon':
          f = new ol.Feature(new ol.geom.Polygon(
            [[  [p[0]-4*r,p[1]-2*r], 
              [p[0]+3*r,p[1]-2*r], 
              [p[0]+1*r,p[1]-0.5*r], 
              [p[0]+4*r,p[1]+2*r], 
              [p[0]-2*r,p[1]+2*r], 
              [p[0]-4*r,p[1]-2*r]
            ]]));
          break;
        case 'Point':
        default:
          f = new ol.Feature(new ol.geom.Point(p));
          break;
      }
      vector.getSource().addFeature(f);
    }

    addFeatureAt([lat, lng]);
} 
</script>
@endsection