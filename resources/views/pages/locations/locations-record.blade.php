@extends('template.main')
@section('stylesheets')
  <!-- CSS DE OPENLAYERS -->
  <link href="{{ asset('css/ol.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('ubicaciones')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Ubicación</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
                <a href="{{ url('/ubicaciones/' . $location->id . '/editar') }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Cliente</span>
                    <span>{{ $location->Client->comercial_name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre</span>
                    <span>{{ $location->name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">ID de Tienda</span>
                    <span>{{ $location->store_id }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">CLAVE de Tienda</span>
                    <span>{{ $location->store_key }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Código postal</span>
                    <span>{{ $location->zipcode }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Colonia</span>
                    <span>{{ $location->colony }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Calle</span>
                    <span>{{ $location->street }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold"># Exterior</span>
                    <span>{{ $location->ext_number }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold"># Interior</span>
                    <span>{{ $location->int_number }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Delegación o municipio</span>
                    <span>{{ $location->city }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Estado</span>
                    <span>{{ $location->state }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Latitud</span>
                    <span>{{ $location->lat }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Longitud</span>
                    <span>{{ $location->long }}</span>
                  </div>
                </div>
                <div class="col-12">
                  <div id="map" class="w-100" style="height: 400px"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scripts')
<!-- JS DE OPENLAYERS -->
<script src="{{ asset('js/ol.js') }}"></script>
<script src="{{ asset('js/ol-ext.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>

<script type="text/javascript">
var lat = 0;
var lng = 0;
$(document).ready(function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  } else {
    alert('La geolocalización no esta activada en este navegador.');
  }   
});

function successFunction(position) {
    lat = {{ $location->lat }};
    lng = {{ $location->long }};
    initMap(lng, lat);
}

function errorFunction(position) {
    alert('Error!');
}

var initMap = function(lng, lat) {
  var map = new ol.Map({
   target: 'map',
   layers: [
     new ol.layer.Tile({
       source: new ol.source.OSM()
     }) 
   ],
   view: new ol.View({
     center: [lat, lng],
     zoom: 18
   })
 });

  var style = 
    [ new ol.style.Style(
        { image: new ol.style.Shadow(
          { radius: 15,
          }),
          stroke: new ol.style.Stroke(
          { color: [0,0,0,0.3],
            width: 2
          }),
          fill: new ol.style.Fill(
            { color: [0,0,0,0.3]
            }),
          zIndex: -1
        }),
      new ol.style.Style(
        { /* image: new ol.style.Icon({ src:"data/camera.png", scale: 0.8 }), */
          image: new ol.style.RegularShape(
          { radius: 10,
            radius2: 5,
            points: 5,
            fill: new ol.style.Fill({ color: 'blue' })
          }),
        stroke: new ol.style.Stroke(
          { color: [0,0,255],
            width: 2
          }),
        fill: new ol.style.Fill(
          { color: [0,0,255,0.3]
          })
        })
    ];
    style[1].getImage().getAnchor()[1] += 10;

    // Vector layer
    var source = new ol.source.Vector();
    var vector = new ol.layer.Vector(
    { source: source,
      style: style
    });
    map.addLayer(vector);

    map.addInteraction(new ol.interaction.Select());

    /* Use filter or opacity * /
      var c = map.getView().getCenter()
      var g = ol.geom.Polygon.fromExtent(ol.extent.buffer(ol.extent.boundingExtent([c]),500000));
      vector.addFilter(new ol.filter.Crop({
        feature: new ol.Feature(g),
        fill: new ol.style.Fill({color: [255,0,0,.5]})
      }));
      vector.setOpacity(.5)
      /**/

      // Add a feature on the map
      function addFeatureAt(p)
      { var f, r = map.getView().getResolution() *10;
        switch ($("#geom").val())
        { case 'LineString':
            f = new ol.Feature(new ol.geom.LineString(
              [ [p[0]-8*r,p[1]-3*r], 
                [p[0]-2*r,p[1]+1*r], 
                [p[0]+2*r,p[1]-1*r], 
                [p[0]+8*r,p[1]+3*r]
              ]));
            break;
          case 'Polygon':
            f = new ol.Feature(new ol.geom.Polygon(
              [[  [p[0]-4*r,p[1]-2*r], 
                [p[0]+3*r,p[1]-2*r], 
                [p[0]+1*r,p[1]-0.5*r], 
                [p[0]+4*r,p[1]+2*r], 
                [p[0]-2*r,p[1]+2*r], 
                [p[0]-4*r,p[1]-2*r]
              ]]));
            break;
          case 'Point':
          default:
            f = new ol.Feature(new ol.geom.Point(p));
            break;
        }
        vector.getSource().addFeature(f);
      }

      addFeatureAt([lat, lng]);
} 
</script>
@endsection