@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                        <a href="{{url('rutas/agregar')}}" class="btn btn-primary">Nuevo</a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Rutas</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="shows" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr class="text-center">
                    <th></th>
                    <th>Chofer</th>
                    <th>Unidad</th>
                    <th>Entregas</th>
                    <th>Fecha</th>
                    <th>Estatus</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($routes as $r)
                  <tr>
                    <td>
                      <a href="{{url('rutas/'.$r->id)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('rutas/'.$r->id.'/editar')}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('rutas/'.$r->id.'/eliminar')}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{ $r->Driver->name . ' ' . $r->Driver->last_name_father . ' ' . $r->Driver->last_name_mother }}</td>
                    <td class="text-center">{{ $r->Vehicle->name }}</td>
                    <td class="text-center">{{ count($r->Detail) }}</td>
                    <td class="text-center">{{ date('d-m-Y', strtotime($r->date)) }}</td>
                    <td class="text-center">{{ $r->Status->name }}</td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#shows').DataTable({
      responsive: true
    });
    $('#shows').show();
    $(window).trigger('resize');
} );
</script>

@endsection