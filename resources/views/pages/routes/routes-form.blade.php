@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('rutas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($route)
                <h1 class="m-0 text-dark">Editar ruta</h1>
              @else
                <h1 class="m-0 text-dark">Agregar ruta</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($route)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row mb-3">
        <div class="col-12">
          @isset($route)
            <form id="form" method="POST" action="{{url('/rutas/'. $route->id)}}">
          @else
            <form id="form" method="POST" action="{{url('/rutas')}}">
          @endisset
          
            @csrf
            <div class="row">
              <div class="col-3">
                <div class="form-group">
                  <label for="id_driver">Chofer</label>
                  <select class="form-control required-select" id="id_driver" name="id_driver">
                      <option value="0">Selecciona un chofer</option>
                    @foreach($drivers as $d)
                      <option value="{{ $d->id }}" {{ isset($route) ? $route->id_driver == $d->id ? "selected" : "" : "" }} data-vehicle="{{ $d->id_vehicle }}">{{ $d->name . ' ' . $d->last_name_father . ' ' . $d->last_name_mother }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="id_vehicle">Vehículo</label>
                  <select class="form-control required-select" id="id_vehicle" name="id_vehicle">
                    <option value="0">Selecciona un vehículo</option>
                    @foreach($vehicles as $v)
                      <option value="{{ $v->id }}" {{ isset($route) ? $route->id_vehicle == $v->id ? "selected" : "" : "" }}>{{ $v->mark . ' - ' . $v->model . ' (' . $v->plates . ') ' }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="date">Fecha</label>
                  <input type="date" class="form-control required" value="{{ isset($route) ? $route->date : date('Y-m-d') }}" id="date"/>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="date">Estatus</label>
                  <select class="form-control required-select" name="id_route_status" id="id_route_status">
                    @foreach($routeStatus as $rS)
                      <option value="{{ $rS->id }}">{{ $rS->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <button class="btn btn-primary" id="add_route" type="button"><i class="fas fa-plus"></i> Agregar ruta</button>
            </div>
          </form>
        </div>
      </div>
      <div id="routes_container">
        @if(!isset($route))
          <div class="card" id="route_1">
            <div class="card-body row">
              <div class="col-3">
                <div class="form-group">
                  <label for="id_client">Cliente</label>
                  <select class="form-control required-select autocomplete" id="id_client_1" name="id_client[]" data-link="{{ url('/clientes/ubicaciones') }}" data-target="id_location_1">
                      <option value="0">Selecciona un cliente</option>
                    @foreach($clients as $c)
                      <option value="{{ $c->id }}" {{ isset($route) ? $route->Location->Client->id == $c->id ? "selected" : "" : "" }}>{{ $c->comercial_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="id_location">Ubicación</label>
                  <select class="form-control required-select" id="id_location_1" name="id_location[]">
                    @isset($locations)
                      @foreach($locations as $l)
                        <option value="{{ $l->id }}" {{ isset($route) ? $route->id_location == $l->id ? "selected" : "" : "" }}>{{ $l->name }}</option>
                      @endforeach
                    @endisset
                  </select>
                </div>
              </div>            
              <div class="col-3">
                <div class="form-group">
                  <label for="packages_1">Número de paquetes</label>
                  <input type="text" class="form-control" name="packages[]" id="packages_1"/>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="id_client">Estatus</label>
                  <select class="form-control required-select" id="id_status_1" name="id_status[]">
                    @foreach($routeStatus as $rS)
                      <option value="{{ $rS->id }}">{{ $rS->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      @else
        @foreach($route->Detail as $rD)
            <div class="card" id="route_{{ $rD->id }}">
              <div class="card-body row">
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_client">Cliente</label>
                    <select class="form-control required-select autocomplete" id="id_client_{{ $rD->id }}" name="id_client[]" data-link="{{ url('/clientes/ubicaciones') }}" data-target="id_location_{{ $rD->id }}">
                        <option value="0">Selecciona un cliente</option>
                      @foreach($clients as $c)
                        <option value="{{ $c->id }}" {{ $rD->id_client == $c->id ? "selected" : "" }}>{{ $c->comercial_name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_location">Ubicación</label>
                    <select class="form-control required-select" id="id_location_{{ $rD->id }}" name="id_location[]">
                      @foreach($rD->Client->Locations as $l)
                        <option value="{{ $l->id }}" {{ $rD->id_location == $l->id ? "selected" : "" }}>{{ $l->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>            
                <div class="col-3">
                  <div class="form-group">
                    <label for="packages_1">Número de paquetes</label>
                    <input type="text" class="form-control" name="packages[]" id="packages_{{ $rD->id }}" value="{{ $rD->packages }}"/>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="id_client">Estatus</label>
                    <select class="form-control required-select" id="id_status_{{ $rD->id }}" name="id_status[]">
                      @foreach($routeStatus as $rS)
                        <option value="{{ $rS->id }}" {{ $rD->id_route_status == $rS->id ? "seleted" : "" }}>{{ $rS->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="row justify-content-end mb-3 mr-3">
                <button type="button" class="btn btn-danger remove-route-form" data-route="{{ $rD->id }}"><i class="fas fa-trash-alt"></i> Eliminar</button>
              </div>
            </div>
          </div>
        @endforeach
      @endif
    </div>
  </section>
</div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/pages/routes/form.js') }}"></script>
@endsection