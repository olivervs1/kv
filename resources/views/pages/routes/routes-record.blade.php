@extends('template.main')
@section('stylesheets')
  <!-- CSS DE OPENLAYERS -->
  <link href="{{ asset('css/ol.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('rutas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Detalle de Ruta</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
                <a href="{{ url('/rutas/' . $route->id . '/editar') }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Chofer</span>
                    <span>{{ $route->Driver->name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Vehículo</span>
                    <span>{{ $route->Vehicle->model . ' - ' . $route->Vehicle->plates }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Fecha</span>
                    <span>{{ date('d-m-Y', strtotime($route->date)) }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Estatus</span>
                    <span>{{ $route->Status->name }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h5>Detalle</h5>
              
                  @if(count($route->Detail) > 0)
                    @foreach($route->Detail as $d)
                      <div class="card">
                        <div class="row">
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Cliente</span>
                            <span>{{ $d->Client->comercial_name }}</span>
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Ubicación</span>
                            <span>{{ $d->Location->name }}</span>
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Paquetes</span>
                            <span>{{ $d->packages }}</span>
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Dirección</span>
                            <span>
                              {{ $d->Location->street . ', # exterior ' . $d->Location->ext_number }}
                                @if($d->Location->int_number != "") 
                                  {{ ', # interior ' . $d->Location->int_number }}
                                @endif
                              {{ ', ' }}
                              <br>
                            {{ $d->Location->colony . ', ' . $d->Location->city . ', ' . $d->Location->state . ', ' . $d->Location->zipcode }}
                            </span>
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Receptor</span>
                            <span>{{ $d->receiver }}</span>
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Evidencia</span>
                            @if($d->evidence != NULL)
                              <img src="{{ $d->evidence }}" height="50px"/>
                            @endif
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Firma</span>
                            @if($d->sign != NULL)
                              <img src="{{ $d->sign }}" height="50px"/>
                            @endif
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Encuesta</span>
                            @if($d->survey != NULL)
                              <ul>
                                @foreach(json_decode($d->survey) as $s)
                                  @php($question = App\Models\Survey::find($s->id))

                                  @if($question instanceof App\Models\Survey)
                                    <li>{{ $question->question }}: {{ $s->score }} <i class="fas fa-star text-warning"></i></li>
                                  @endif
                                @endforeach
                              </ul>
                            @endif
                          </div>
                          <div class="col-4">
                            <span class="d-block font-weight-bold">Estatus</span>
                            <span>
                              {{ $d->Status->name }} 
                              <i class="fas fa-circle" style="color: {{ $d->Status->color  }}"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="3" class="text-center">Sin ubicaciones aún</td>
                    </tr>
                  @endif
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h5>Mapa de la ruta</h5>
              <div id="map" class="w-100" style="height: 400px"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<!-- JS DE OPENLAYERS -->
<script src="{{ asset('js/ol.js') }}"></script>
<script src="{{ asset('js/ol-ext.js') }}"></script>

<script type="text/javascript">
var lat = 0;
var lng = 0;
$(document).ready(function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  } else {
    alert('La geolocalización no esta activada en este navegador.');
  }   
});

function successFunction(position) {
    lat = {{ $route->Detail[1]->Location->lat }};
    lng = {{ $route->Detail[1]->Location->long }};
    initMap(lng, lat);
}

function errorFunction(position) {
    alert('Error!');
}

var initMap = function(lng, lat) {
  var map = new ol.Map({
   target: 'map',
   layers: [
     new ol.layer.Tile({
       source: new ol.source.OSM()
     }) 
   ],
   view: new ol.View({
     center: [lat, lng],
     zoom: 12
   })
 });

  var style = 
    [ new ol.style.Style(
        { image: new ol.style.Shadow(
          { radius: 15,
          }),
          stroke: new ol.style.Stroke(
          { color: [0,0,0,0.3],
            width: 2
          }),
          fill: new ol.style.Fill(
            { color: [0,0,0,0.3]
            }),
          zIndex: -1
        }),
      new ol.style.Style(
        { /* image: new ol.style.Icon({ src:"data/camera.png", scale: 0.8 }), */
          image: new ol.style.RegularShape(
          { radius: 10,
            radius2: 5,
            points: 5,
            fill: new ol.style.Fill({ color: 'blue' })
          }),
        stroke: new ol.style.Stroke(
          { color: [0,0,255],
            width: 2
          }),
        fill: new ol.style.Fill(
          { color: [0,0,255,0.3]
          })
        })
    ];
    style[1].getImage().getAnchor()[1] += 10;

    // Vector layer
    var source = new ol.source.Vector();
    var vector = new ol.layer.Vector(
    { source: source,
      style: style
    });
    map.addLayer(vector);

    map.addInteraction(new ol.interaction.Select());

    /* Use filter or opacity * /
      var c = map.getView().getCenter()
      var g = ol.geom.Polygon.fromExtent(ol.extent.buffer(ol.extent.boundingExtent([c]),500000));
      vector.addFilter(new ol.filter.Crop({
        feature: new ol.Feature(g),
        fill: new ol.style.Fill({color: [255,0,0,.5]})
      }));
      vector.setOpacity(.5)
      /**/

      // Add a feature on the map
      function addFeatureAt(p) { 
        var f, r = map.getView().getResolution() *10;
        f = new ol.Feature(new ol.geom.Point(p));
        
        vector.getSource().addFeature(f);
      }

      @foreach($route->Detail as $d)
        @if($d->Location->lat != "")
          addFeatureAt([{{ $d->Location->lat }}, {{ $d->Location->long }}]);
        @endif
      @endforeach
} 
</script>
@endsection