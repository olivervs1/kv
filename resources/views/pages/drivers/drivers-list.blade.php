@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('choferes/agregar')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Choferes</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="shows" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Número de empleado</th>
                    <th>Nombre completo</th>
                    <th>Correo electrónico</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($drivers as $d)
                  <tr>
                    <td>
                      <a href="{{url('choferes/'.$d->id)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('choferes/'.$d->id.'/editar')}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('choferes/'.$d->id.'/eliminar')}}"><i class="far fa-trash-alt text-danger mr-2"></i></a>
                      <a href="{{url('choferes/'.$d->id.'/chat')}}" class="text-muted"><i class="fas fa-comments"></i></a>
                    </td>
                    <td>{{$d->employee_number}}</td>
                    <td>{{$d->name . ' ' . $d->last_name_father . ' ' . $d->last_name_mother }}</td>
                    <td>{{$d->email }}</td>
                    <td>
                        <a href="" class="text-reset">
                            
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#shows').DataTable({
      responsive: true
    });
    $('#shows').show();
    $(window).trigger('resize');
} );
</script>

@endsection
