@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('choferes')}}"><div class='btn btn-primary'>Listado</div></a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Conversación con  {{ $driver->name . ' ' . $driver->lastname }}</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="chat-container rounded bg-white mh-50 mb-3 form-control">
                    @php($admin = Auth::user()->id)
                    @foreach($messages as $m)
                        <div class="row mb-2">
                            <div class="{{ $admin == $m->sender_id ? 'offset-9 my_message' : ''}} col-3">
                                <div class="chat-message">
                                    {{ $m->message }}
                                </div>
                                <small class="font-italic">Enviar por {{ $m->Sender->name }} - {{ date('d-m-Y H:i:s', strtotime($m->created_at)) }}</small>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="chat-controls">
                    <form id="chat_message" action="{{ url('/choferes/' . $driver->id . '/chat/enviar-mensaje') }}" method="POST" class="row col-8 offset-2">
                        <div class="form-group col-10">
                            <textarea class="form-control" placeholder="Mensaje..." name="message" id="message"></textarea>
                        </div>
                        <div class="col-2 d-flex pb-3">
                            <button type="button" class="btn btn-primary" id="send_message">Enviar <i class="fas fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<input type="hidden" id="user_name" value="{{ Auth::user()->name }}"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/pages/drivers/chat.js') }}"></script>
@endsection
