@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('choferes')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Chofer</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
                <a href="{{ url('/choferes/' . $driver->id . '/editar') }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Nombre</span>
                    <span>{{ $driver->name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Apellido paterno</span>
                    <span>{{ $driver->last_name_father }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Apellido materno</span>
                    <span>{{ $driver->last_name_mother }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Número de empleado</span>
                    <span>{{ $driver->employee_number }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Teléfono</span>
                    <span>{{ $driver->phone }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Correo electrónico</span>
                    <span>{{ $driver->email }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Vehículo predeterminado</span>
                    @if($driver->Vehicle != NULL)
                      <span>{{ $driver->Vehicle->mark . ' - ' . $driver->Vehicle->model . ' - ' . $driver->Vehicle->plates }}</span>
                    @else
                      <span>Aún no se ha asignado ningún vehículo</span>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
@endsection