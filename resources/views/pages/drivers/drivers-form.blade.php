@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('choferes')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($driver)
                <h1 class="m-0 text-dark">Editar chofer</h1>
              @else
                <h1 class="m-0 text-dark">Agregar chofer</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($driver)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($driver)
                <form id="form" method="POST" action="{{url('/choferes/'. $driver->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/choferes')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre" value="{{ isset($driver) ? $driver->name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="last_name_father">Apellido paterno</label>
                      <input type="text" class="form-control" id="last_name_father" name="last_name_father" placeholder="Apellido paterno" value="{{ isset($driver) ? $driver->last_name_father : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="last_name_mother">Apellido materno</label>
                      <input type="text" class="form-control" id="last_name_mother" name="last_name_mother" placeholder="Apellido materno" value="{{ isset($driver) ? $driver->last_name_mother : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="employee_number">Número de empleado</label>
                      <input type="text" class="form-control required" id="employee_number" name="employee_number" placeholder="Número de empleado" value="{{ isset($driver) ? $driver->employee_number : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="phone">Teléfono</label>
                      <input type="text" class="form-control" id="phone" name="phone" placeholder="Teléfono" value="{{ isset($driver) ? $driver->phone : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="email">Correo electrónico</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="Correo electrónico" value="{{ isset($driver) ? $driver->email : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="email">Vehículo predeterminado</label>
                      <select name="id_vehicle" class="form-control">
                        <option value="0">Selecciona un vehículo</option>
                        @foreach($vehicles as $v)
                          <option value="{{ $v->id }}" {{ isset($driver) ? $driver->id_vehicle == $v->id ? "selected" : ""  : "" }}>{{ $v->mark . ' - ' . $v->model . ' - ' . $v->plates  }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection