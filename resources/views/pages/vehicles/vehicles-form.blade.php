@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('vehiculos')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($vehicle)
                <h1 class="m-0 text-dark">Editar vehículo</h1>
              @else
                <h1 class="m-0 text-dark">Agregar vehículo</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($vehicle)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($vehicle)
                <form id="form" method="POST" action="{{url('/vehiculos/'. $vehicle->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/vehiculos')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre de la unidad</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre" value="{{ isset($vehicle) ? $vehicle->name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="mark">Marca</label>
                      <input type="text" class="form-control" id="mark" name="mark" placeholder="Marca" value="{{ isset($vehicle) ? $vehicle->mark : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="model">Modelo</label>
                      <input type="text" class="form-control" id="model" name="model" placeholder="Modelo" value="{{ isset($vehicle) ? $vehicle->model : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="year">Año</label>
                      <input type="text" class="form-control" id="year" name="year" placeholder="Año" value="{{ isset($vehicle) ? $vehicle->year : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="plates">Número de placas</label>
                      <input type="text" class="form-control required" id="plates" name="plates" placeholder="Número de placas" value="{{ isset($vehicle) ? $vehicle->plates : ''}}">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection