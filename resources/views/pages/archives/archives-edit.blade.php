@extends('template.main')
@section('stylesheets')
@endsection
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('carta-porte')}}"><div class='btn btn-primary'>Listado</div></a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Archivo: Carta Porte</h1>
                    </div>
                      <div class="col-4 text-right">
                        <div class='btn btn-success float-right' onclick="$('form#edit_file').submit()">Actualizar</div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                        <form id="edit_file" class="row col-12" enctype="multipart/form-data" method="POST" action="{{ url('/carta-porte/' . $archive->id) }}">
                            @csrf
                            <div class="form-group col">
                                <label for="pdf">
                                    PDF
                                </label>

                                @if($archive->pdf != "")
                                    <span class="d-block">{{ $archive->pdf }} <i class="text-success fa fa-check"></i></span>
                                @else
                                    <span class="d-block">&nbsp;</span>
                                @endif
                                <input type="file" name="pdf" class="form-control"/>
                            </div>
                            <div class="form-group col">
                                <label for="xml">
                                    XML
                                </label>
                                @if($archive->xml != "")
                                    <span class="d-block">{{ $archive->xml }} <i class="text-success fa fa-check"></i></span>
                                @else
                                    <span class="d-block">&nbsp;</span>
                                @endif
                                <input type="file" name="xml" class="form-control"/>
                            </div>
                            <div class="form-group col">
                                <label for="client_id">
                                    Cliente
                                </label>
                                <span class="d-block">&nbsp;</span>
                                <select class="form-control" name="client_id">
                                    <option value="0">Selecciona un cliente</option>
                                    @foreach($clients as $c)
                                        <option value="{{ $c->id }}" {{ $c->id == $archive->client_id ? 'selected' : '' }}>{{ $c->comercial_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                      </div>
                      <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
@endsection
