@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('carta-porte/agregar')}}"><div class='btn btn-primary'>Nuevo</div></a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Archivo: Carta Porte</h1>
                    </div>
                      <div class="col-4 text-right">
                        <button class="btn btn-secondary" data-toggle="modal" data-target="#download_archive">
                            Descargar <i class="fas fa-download"></i>
                        </button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-2">
                        Desde
                        <input type="date" id="search_from" class="form-control" value="{{ isset($_GET['from']) ? $_GET['from'] : ''}}" />
                    </div>
                    <div class="col-2">
                        Hasta
                        <input type="date" id="search_to" class="form-control" value="{{ isset($_GET['to']) ? $_GET['to'] : ''}}" />
                    </div>
                    <div class="col-2">
                        <button type="button" id="search" class="btn btn-primary d-block">Buscar <i class="fas fa-search"></i></button>
                    </div>
                </div>
                <table id="files" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th>PDF</th>
                    <th>XML</th>
                    <th>Cliente</th>
                    <th>Subtotal</th>
                    <th>Total</th>
                    <th>Fecha</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($archives as $a)
                  <tr>
                    <td>
                      <a href="{{url('carta-porte/'.$a->id.'/editar')}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('carta-porte/'.$a->id.'/eliminar')}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td class="align-middle">
                        @if($a->pdf != NULL)
                            {{ $a->pdf }} <i class="text-success fa fa-check"></i> 
                        @else
                            <i class="text-danger fa fa-times"></i>
                        @endif
                    </td>
                    <td class="align-middle">
                        @if($a->xml != NULL)
                            {{ $a->xml }} <i class="text-success fa fa-check"></i> 
                        @else
                            <i class="text-danger fa fa-times"></i>
                        @endif
                    </td>
                    <td>
                        @if($a->Client != NULL)
                            {{ $a->Client->comercial_name }}
                        @endif
                    </td>
                    <td class="text-right">    
                        @if($a->xml != NULL)
                            ${{ number_format(get_object_vars(json_decode($a->notes)->subtotal)[0],2,'.',',') }}
                        @endif
                    </td>
                    <td class="text-right">    
                        @if($a->xml != NULL)
                            ${{ number_format(get_object_vars(json_decode($a->notes)->total)[0],2,'.',',') }}
                        @endif
                    </td>
                    <td>
                        @if($a->date != NULL)
                            {{ date('d-m-Y H:i:s', strtotime($a->date)) }}
                        @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div class="modal fade" id="download_archive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Generar archivo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="files_form" action="{{ url('/carta-porte/descargar') }}">
            @csrf
            <div class="form-group">
                <label for="client">Cliente</label>
                <select class="form-control" id="client_id" name="client_id">
                    <option value="0">Selecciona un cliente</option>
                    @foreach($clients as $c)
                        <option value="{{ $c->id }}">{{ $c->comercial_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Desde</label>
                <input type="date" class="form-control" name="from" id="from"/>
            </div>
            <div class="form-group">
                <label>Hasta</label>
                <input type="date" class="form-control" name="to" id="to" value="{{ date('Y-m-d') }}" />
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="download" class="btn btn-primary">Descargar <i class="fas fa-file-archive ml-2"></i></button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="url" value="{{ url()->current() }}"/>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/pages/archives/list.js') }}"></script>

    <script>
        $(document).ready( function () {
        $('#files').DataTable({
          responsive: true,
          dom: 'Bfrtip',
          buttons: [
              'excel', 'pdf', 'print'
          ]
        });
        $('#files').show();
        $(window).trigger('resize');
    } );
    </script>
@endsection

