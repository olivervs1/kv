@extends('template.main')
@section('stylesheets')
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                  <a href="{{url('carta-porte')}}"><div class='btn btn-primary'>Listado</div></a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Archivo: Carta Porte</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                        <div class="form-group">
                            <label for="client">Cliente</label>
                            <select class="form-control" id="client_id" name="client_id">
                                <option value="0">Selecciona un cliente</option>
                                @foreach($clients as $c)
                                    <option value="{{ $c->id }}">{{ $c->comercial_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <form class="dropzone" id="archives-dropzone" enctype="multipart/form-data" method="POST" action="{{ url('/carta-porte') }}" style="display: none;">
                            @csrf
                            <div class="dz-message" data-dz-message>
                                <span class="fs-2">Arrastra y suelta aquí los archivos que deseas archivar. <i class="fa fa-upload"></i></span>
                            </div>
                            <input type="file" multiple class="form-control d-none" name="files"/>          
                            <input type="text" class="d-none" name="client_id" id="dropzone_client_id"/>          
                        </form>
                      </div>
                      <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <script src="{{ asset('/js/pages/archives/form.js') }}"></script>
@endsection
