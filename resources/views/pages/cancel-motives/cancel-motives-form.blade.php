@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('motivos-de-cancelacion')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($motive)
                <h1 class="m-0 text-dark">Editar</h1>
              @else
                <h1 class="m-0 text-dark">Agregar</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($motive)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($motive)
                <form id="form" method="POST" action="{{url('/motivos-de-cancelacion/'. $motive->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/motivos-de-cancelacion')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Motivos</label>
                      <input type="text" class="form-control required" id="motive" name="motive" placeholder="Motivo de cancelación" value="{{ isset($motive) ? $motive->motive : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Estatus</label>
                      <select name="status" class="form-control">
                        <option value="1" {{ isset($motive) ? $motive->status == 1 ? "selected" : "" : "" }}>Activo</option>
                        <option value="0" {{ isset($motive) ? $motive->status == 0 ? "selected" : "" : "" }}>No Activo</option>
                      </select>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection