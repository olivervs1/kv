@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('tipos-de-rutas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($routeType)
                <h1 class="m-0 text-dark">Editar tipo de ruta</h1>
              @else
                <h1 class="m-0 text-dark">Agregar tipo de ruta</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($routeType)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($routeType)
                <form id="form" method="POST" action="{{url('/tipos-de-rutas/'. $routeType->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/tipos-de-rutas')}}">
              @endisset
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre" value="{{ isset($routeType) ? $routeType->name : ''}}">
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection