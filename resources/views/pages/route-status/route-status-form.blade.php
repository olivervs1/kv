@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('estatus-rutas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($routeStatus)
                <h1 class="m-0 text-dark">Editar estatus</h1>
              @else
                <h1 class="m-0 text-dark">Agregar estatus</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($routeStatus)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($routeStatus)
                <form id="form" method="POST" action="{{url('/estatus-rutas/'. $routeStatus->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/estatus-rutas')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Estatus</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre" value="{{ isset($routeStatus) ? $routeStatus->name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="mark">Color</label>
                      <input type="color" class="form-control" id="color" name="color" value="{{ isset($routeStatus) ? $routeStatus->color : ''}}">
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection