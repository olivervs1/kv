@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('estatus-rutas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              <h1 class="m-0 text-dark">Estatus de ruta</h1>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
                <a href="{{ url('/estatus-rutas/' . $routeStatus->id . '/editar') }}" class='btn btn-success float-right'>Editar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Estatus</span>
                    <span>{{ $routeStatus->name }}</span>
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                    <span class="d-block font-weight-bold">Color</span>
                    <span class="d-block alert mb-0" style="background-color: {{ $routeStatus->color  }}"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
@endsection