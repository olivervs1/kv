@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                        <a href="{{url('rutas-preconfiguradas/agregar')}}" class="btn btn-primary">Nuevo</a>                     
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Rutas Preconfiguradas</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="shows" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr class="text-center">
                    <th></th>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Ubicaciones</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($routes as $r)
                  <tr>
                    <td>
                      <a href="{{url('rutas-preconfiguradas/'.$r->id)}}"><i class="far fa-eye text-secondary mr-2"></i></a>
                      <a href="{{url('rutas-preconfiguradas/'.$r->id.'/editar')}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('rutas-preconfiguradas/'.$r->id.'/eliminar')}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td>{{ $r->name }}</td>
                    <td class="text-center">{{ $r->RouteType->name }}</td>
                    <td class="text-center">{{ count($r->Locations) }}</td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#shows').DataTable({
      responsive: true
    });
    $('#shows').show();
    $(window).trigger('resize');
} );
</script>

@endsection