@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('rutas-preconfiguradas')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($route)
                <h1 class="m-0 text-dark">Editar ruta</h1>
              @else
                <h1 class="m-0 text-dark">Agregar ruta</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($route)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($route)
                <form id="form" method="POST" action="{{url('/rutas-preconfiguradas/'. $route->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/rutas-preconfiguradas')}}">
              @endisset
                @csrf
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Nombre</label>
                      <input type="text" class="form-control required" id="name" name="name" placeholder="Nombre" value="{{ isset($route) ? $route->name : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Tipo de ruta</label>
                      <select class="form-control" name="route_type_id" id="route_type_id">
                        @foreach($route_types as $r)
                          <option value="{{ $r->id }}">{{ $r->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row col-12 text-center">
                  <div class="form-group">
                    <span class="btn btn-primary" id="add_location">
                      <i class="fas fa-plus"></i> Agregar ubicación
                    </span>
                  </div>
                </div>
                <div class="row col-12" id="routes_container">
                  @if(!isset($route) || $route->Locations->count() == 0)
                    <div class="location border-rounded shadow mb-4 col-12 p-2" id="route_1">
                      <div class="form-group row">
                        <div class="col-5">
                          <label>Cliente</label>
                          <select class="form-control client autocomplete" id="id_client_1" name="id_client[]" data-link="{{ url('/clientes/ubicaciones') }}" data-target="id_location_1">
                            <option value="0">--- Seleccionar cliente ---</option>
                            @foreach($clients as $c)
                              <option value="{{ $c->id }}">{{ $c->comercial_name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-5">
                          <label>Ubicación</label>
                          <select class="form-control location" id="id_location_1"  name="id_location[]"></select>
                        </div>
                      </div>
                    </div>
                  @else
                    @foreach($route->Locations as $loc)
                      <div class="location border-rounded shadow mb-4 col-12 p-2" id="route_{{ $loc->id }}">
                        <div class="form-group row">
                          <div class="col-5">
                            <label>Cliente</label>
                            <select class="form-control client autocomplete" id="id_client_{{ $loc->client_id }}" name="id_client[]" data-link="{{ url('/clientes/ubicaciones') }}" data-target="id_location_{{ $loc->location_id }}">
                              <option value="0">--- Seleccionar cliente ---</option>
                              @foreach($clients as $c)
                                <option value="{{ $c->id }}" {{ $loc->client_id == $c->id ? 'selected' : '' }}>{{ $c->comercial_name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-5">
                            <label>Ubicación</label>
                            <select class="form-control location" id="id_location_{{ $loc->location_id }}"  name="id_location[]">
                              @foreach($loc->Client->Locations as $l)
                                <option value="{{ $l->id }}" {{ $loc->location_id == $l->id ? 'selected' : '' }}>{{ $l->store_id . ' - ' . $l->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="row justify-content-end mb-3 mr-3">
                          <button type="button" class="btn btn-danger remove-route-form" data-route="{{ $loc->id }}"><i class="fas fa-trash-alt"></i> Eliminar</button>
                        </div>
                      </div>
                    @endforeach
                  @endif
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pages/preconfigured/form.js') }}"></script>
@endsection