@extends('template.main')
@section('content')
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-6">
                      
                  <h1 class="m-0 text-dark">Remisiones de Gameplanet</h1>
                    </div>
                      <div class="col-4 text-right">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card justify-content-center col-12">
                        <div class="col-4 text-center offset-md-4 p-3">
                            <button class="btn btn-primary" role="button" onclick="$('div#get_new_remission').modal()">
                                Nueva remisión
                            </button>
                        </div>
              <!-- /.card-header -->
              <!-- <div class="card-body">
                <table id="shows" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th></th>
                    <th># de Remisión</th>
                    <th># de Ruta</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($files as $f)
                  <tr>
                    <td>
                      <a href="{{url('/gameplanet/'.$f->id.'/editar')}}"><i class="far fa-edit text-info mr-2"></i></a>
                      <a href="{{url('/gameplanet/'.$f->id.'/imprimir')}}"><i class="far fa-excel text-info mr-2"></i></a>
                      <a href="{{url('/gameplanet/'.$r->id.'/eliminar')}}"><i class="far fa-trash-alt text-danger"></i></a>
                    </td>
                    <td class="align-middle">{{ $f->remision }}</td>
                    <td class="align-middle">{{ $f->ruta }}</span></td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div> -->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal" tabindex="-1" role="dialog" id="get_new_remission">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nueva remisión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="remission_form" action="{{ url('/gameplanet/nueva-remision') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="remission_number">Número de remisión (Separadas por coma)</label>
                <input type="text" class="form-control" id="remission_number" name="remission_number" placeholder="EJ. XXXX-XXXXX, YYYY-YYYYY, ZZZZ-ZZZZZZ">
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <input type="radio" class="type_selection" name="get_by" value="region" id="opt_region"/> 
                        <label for="opt_region">Región</label>
                    </div>
                    <div class="col">
                        <input type="radio" class="type_selection" name="get_by" value="route" id="opt_route"/> 
                        <label for="opt_route">Ruta</label>
                    </div>
                    <div class="col">
                        <input type="radio" class="type_selection" name="get_by" value="store" id="opt_store"/> 
                        <label for="opt_store">Tienda</label>
                    </div>
                </div>
            </div>
            <div class="form-group" id="region_selection" style="display: none;">
                <label for="region_type">Región</label>
                <select class="form-control" id="region_type" name="region_type">
                    @foreach($routeTypes as $rT)
                        <option value="{{ $rT->id }}">{{ $rT->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="route_selection" style="display: none;">
                <label for="route_number">Ruta</label>
                <select class="form-control" id="route_number" name="route_number">
                    @foreach($routes as $r)
                        <option value="{{ $r->id }}">{{ $r->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="store_selection" style="display: none;">
                <label for="store_number">Tienda</label>
                <select class="form-control" id="store_number" name="store_number">
                    <option value="0">Todas</option>
                    @php($stores = array())
                    @foreach($routes as $r)
                        @foreach($r->Locations as $l)
                            @php($stores[$l->Location->store_id] = $l->Location)
                        @endforeach
                    @endforeach

                    @php(ksort($stores))
                    
                    @foreach($stores as $s)
                        <option value="{{ $s->id }}">{{ $s->store_id }} - {{ $s->name }}</option>
                    @endforeach
                </select>
            </div>
        </form>
        <span class="error alert alert-danger m-2" style="display: none;"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="download-remission">Descargar <i class="fas fa-file-excel"></i></button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
    $('#shows').DataTable({
      responsive: true
    });
    $('#shows').show();
    $(window).trigger('resize');
} );
</script>

<script type="text/javascript" src="{{ asset('/js/pages/gameplanet/index.js') }}"></script>

@endsection