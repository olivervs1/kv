@extends('template.main')
@section('content')
<div class="content-wrapper">

  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-2">
              <a href="{{url('encuesta')}}"><div class='btn btn-secondary'>Listado</div></a>                   
            </div>
            <div class="col-10">
              @isset($question)
                <h1 class="m-0 text-dark">Editar pregunta</h1>
              @else
                <h1 class="m-0 text-dark">Agregar pregunta</h1>
              @endisset
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="row">
            <div class="col-12 ">
              @isset($question)
                <div class='btn btn-success float-right' id="submit">Actualizar</div>
              @else
                <div class='btn btn-success float-right' id="submit">Guardar</div>
              @endisset
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              @isset($question)
                <form id="form" method="POST" action="{{url('/encuesta/'. $question->id)}}">
              @else
                <form id="form" method="POST" action="{{url('/encuesta')}}">
              @endisset
              
                @csrf
                <div class="row">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="name">Pregunta</label>
                      <input type="text" class="form-control required" id="question" name="question" placeholder="Pregunta" value="{{ isset($question) ? $question->question : ''}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="name">Estatus</label>
                      <select name="status" class="form-control">
                        <option value="1" {{ isset($question) ? $question->status == 1 ? "selected" : "" : "" }}>Activo</option>
                        <option value="0" {{ isset($question) ? $question->status == 0 ? "selected" : "" : "" }}>Inactivo</option>
                      </select>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/form/validation.js') }}"></script>
@endsection