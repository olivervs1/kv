$(document).ready(function() {
	$('#submit').on('click', submitForm);
});

var submitForm(e) {
	e.preventDefault();
	
	var add = true;

	$.each($('form input.required'), function(i, e) {
		var $e = $(e);

		if($e.val() == "" || $e.val().length == 0 || $(e).val() == undefined) {
			add = false;
			$(e).addClass('border').addClass('border-danger');
		}
	})

	$.each($('form select.required-select'), function(i, e) {
		var $e = $(e).val();

		if($e.val() == "0" || $e.val() == 0) {
			add = false;
			$(e).addClass('border').addClass('border-danger');
		}
	});

	if(add) {
		$.ajax({
		    headers: {
		        'X-CSRF-TOKEN': $('input[name="_token"]').val()
		    },
		    method: 'POST',
		    url: $('form#form').attr('action'),
		    data: new FormData($('form#form')[0]),
		    dataType:'json',
		    processData: false,
		    contentType: false,
		    success:function(response){                 
		        
		        if(response.message) {
		            createAlert(response.message);

		            $('.alertWindowOk').on('click', function() {
		                if(response.status) {
		                	if(response.redirect) {
		                		document.location = redirect;	
		                	}
		                }
		            });
		        }
		    },
		});
	}
}