//var url = 'http://localhost/kv/public/';
var url = 'http://kv.1808.studio/public/';

$(document).ready(function() {

});

function createAlert(text, title, icon, buttons, ok, cancel, top, overlay) {
	if(title == undefined || title == "") {
		title = "Alerta";
	}

	if(icon == undefined || icon == "") {
		icon = '';
	}

	if(buttons == undefined || buttons == "") {
		buttons = 1;
	}

	if(ok == undefined || ok == "") {
		ok = "Aceptar";
	}

	if(cancel == undefined || cancel == "") {
		cancel = "Cancelar";
	}
	
	var btnCancel = "";

	$(':focus').blur();
	var $overlay = $('<div>', {'class' : 'overlay'});
	if(overlay != undefined && overlay != "") {
		$overlay.css('opacity', 0);
	}
	var $alertWindow = $('<div>', {'class': 'alertWindow green-card'});
	if(top != undefined && top != "") {
		$alertWindow.css('top', top);
	}
	var $wdwHeader = $('<h3>', {'class' : 'header'});
	var $wdwText = $('<p>', {'class' : 'wdwtext'});
	$('body').append($overlay);
	$overlay.css('height', $(document).height());
	var dw = Math.round(document.body.clientWidth/2) - 200;
	var dh = Math.round($(document).height()/2) - 200;
	$alertWindow.css({'left' : dw});
	var btn = $('<input type="button">').val(ok).addClass('alertWindowOk btn btn-primary btn-sm col-sm-3');
	var row = $('<div>').addClass('row justify-content-md-center col-12');
	if(buttons == 2) {
		$alertWindow.css({'min-width': '525px'});
		btnCancel = $('<input type="button">').val(cancel).addClass('alertWindowCancel btn btn-danger btn-sm col-5 fs-14');
		btnCancel.on('click', closeWindow);
		btn = $('<input type="button">').val(ok).addClass('alertWindowOk btn btn-primary col-5 fs-14 btn-sm');
	}
	btn.on('click', closeWindow);
	$('body').append($alertWindow.append($wdwHeader.html(icon).append(title)).append($wdwText.html(text)).append(row.append(btn).append(btnCancel)));

}

function overlay() {
	if($('div.overlay').length < 1) {
		var $div = $('<div>', {'class' : 'overlay'});
		$('body').append($div);
	}
}

function closeWindow() {
	$('div.overlay').remove();
	$('div.alertWindow').remove();
	//$('div.modal').fadeOut();
	$('div.instructions').remove();
}
