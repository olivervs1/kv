var xhr = "";
var user_name = $('input#user_name').val();

$(document).ready(function() {
	$('button#send_message').on('click', sendMessage);
	$(".chat-container").scrollTop($(".chat-container").prop("scrollHeight"));
});

var sendMessage = function() {
	var message = $('textarea#message').val();

	if(xhr == "") {
		if(message != undefined || message != "") {
			xhr = $.ajax({
			    method: 'POST',
			    url: $('form#chat_message').attr('action'),
			    data: new FormData($('form#chat_message')[0]),
			    dataType:'json',
			    processData: false,
				contentType: false,
				success:function(response){	
					if(response.status) {
						xhr = "";
						
						var html = '<div class="row mb-2">';
						html += '<div class="offset-9 my_message col-3">';
						html += '<div class="chat-message">';
						html += message;
						html +='</div>';
						html += '<small class="font-italic">Enviado por ' + user_name + '</small>';
						html +='</div>';
						html += '</div>';

						$('div.chat-container').append(html);
						$('textarea#message').val("");

						$(".chat-container").scrollTop($(".chat-container").prop("scrollHeight"));
					}
				},
				error: function() {
					xhr = "";
				},
			});
		}
	}
}
