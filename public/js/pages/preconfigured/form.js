$(document).ready(function() {
	$('span#add_location').on('click', addRouteForm);
	$('div#submit').on('click', save);
	$('button.remove-route-form').on('click', removeRouteForm);
});

var addRouteForm = function() {
	var routeForm = $('div#routes_container div.location:first-child').clone();
	var idx = $('div#routes_container div.location').length + 1;

	if($('div#route_' + idx).length > 0) {
		while($('div#route_' + idx).length == 0) {
			idx = idx++;
		}
	}
	var html = '<div class="location border-rounded shadow mb-4 p-2 col-12" id="route_' + idx + '">';
	routeForm = routeForm.html().replaceAll('_1','_' + idx);
	html += routeForm;
	html += '<div class="row justify-content-end mb-3 mr-3">';
	html += '<button type="button" class="btn btn-danger remove-route-form" data-route="' + idx +' "><i class="fas fa-trash-alt"></i> Eliminar</button>';
	html += '</div>';
	html += '</div>';

	$('div#routes_container').append(html);	

	$('select#id_client_' + idx + ' option:selected').prop("selectedIndex", 0);
	$('select#id_location_' + idx).html("");
	$('input#packages_' + idx).val("");

	$('select#id_client_' + idx).on('change', autocompleteData);
	$('button.remove-route-form').on('click', removeRouteForm);
}

var removeRouteForm = function() {
	createAlert('¿Estás seguro que deseas eliminar esta ruta?', '', '', 2, 'Si', 'No');
	var id = $(this).data('route');
	
	$('.alertWindowOk').on('click', function() {
		$('div#route_' + id).remove();
	});
}

var save = function(e) {
	e.preventDefault();

	var route_name = $('input#name').val().trim();
	var route_type = $('select#route_type_id').val();
	var add = true;

	if(route_name == "" || route_name == undefined || route_name == null) {
		add = false;
		$('input#route_name').addClass('border-danger');
	}

	if(route_type == "" || route_type == 0) {
		add = false;
		$('select#route_type_id').addClass('border-danger');
	}
	
	if(add) {
		var routes = [];
		$.each($('div#routes_container div.location'), function(i, elem) {
			var id = $(elem).attr('id').replace('route_','');

			var id_client = $('select#id_client_' + id).val();
			var id_location = $('select#id_location_' + id).val();

			if(id_client == 0 || id_client == undefined) {
				createAlert('Debes seleccionar un cliente válido.');
				$('select#id_client_' + id).addClass('border-danger');
				add = false;
				return 0;
			}

			if(id_location == 0 || id_location == undefined) {
				createAlert('Debes seleccionar una ubicación válida.');
				$('select#id_location_' + id).addClass('border-danger');
				add = false;
				return 0;
			}

			var route = {};
			route.id_client = id_client;
			route.id_location = id_location;

			routes.push(route);
		});

		if(routes.length > 0 && add) {
			var data = new FormData;
			data.append('route_name', route_name);
			data.append('route_type', route_type);
			data.append('routes', JSON.stringify(routes));

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('input[name="_token"]').val()
				},
			    method: 'POST',
			    url: $('form#form').attr('action'),
			    data: data,
			    dataType:'json',
			    processData: false,
				contentType: false,
				success:function(response){	
					if(response.message) {
						createAlert(response.message);
						
						$('.alertWindowOk').on('click', function() {
							if(response.redirect) {
								document.location = response.redirect;
							}
						})
					}
				},
			});
		} else {
			createAlert('No has agregado ninguna ruta.');
		}
	}
}