$(document).ready(function() {
	$('#submit').unbind('click');

	$('select#id_driver').on('change', selectVehicle);
	$('button#add_route').on('click', addRouteForm);
	$('div#submit').on('click', save);
	
	$('button.remove-route-form').on('click', removeRouteForm);
});

var selectVehicle = function() {
	var id_vehicle = $('select#id_driver option:selected').data('vehicle');

	$('select#id_vehicle option[value=' + id_vehicle + ']').prop('selected', 'selected');
}

var addRouteForm = function() {
	var routeForm = $('div#routes_container div.card:first-child').clone();
	var idx = $('div#routes_container div.card').length + 1;

	if($('div#route_' + idx).length > 0) {
		while($('div#route_' + idx).length == 0) {
			idx = idx++;
		}
	}
	var html = '<div class="card" id="route_' + idx + '">';
	routeForm = routeForm.html().replaceAll('_1','_' + idx);
	html += routeForm;
	html += '<div class="row justify-content-end mb-3 mr-3">';
	html += '<button type="button" class="btn btn-danger remove-route-form" data-route="' + idx +' "><i class="fas fa-trash-alt"></i> Eliminar</button>';
	html += '</div>';
	html += '</div>';

	$('div#routes_container').append(html);	

	$('select#id_client_' + idx + ' option:selected').prop("selectedIndex", 0);
	$('select#id_location_' + idx).html("");
	$('input#packages_' + idx).val("");

	$('select#id_client_' + idx).on('change', autocompleteData);
	$('button.remove-route-form').on('click', removeRouteForm);
}

var removeRouteForm = function() {
	createAlert('¿Estás seguro que deseas eliminar esta ruta?', '', '', 2, 'Si', 'No');
	var id = $(this).data('route');
	
	$('.alertWindowOk').on('click', function() {
		$('div#route_' + id).remove();
	});
}

var save = function(e) {
	e.preventDefault();

	var id_driver = $('select#id_driver').val();
	var id_vehicle = $('select#id_vehicle').val();
	var id_route_status = $('select#id_route_status').val();
	var date = $('input#date').val();
	var add = true;

	if(id_driver == 0 || id_driver == undefined) {
		createAlert('Debes seleccionar un chofer');
		add = false;
		return 0;
	}

	if(id_vehicle == 0 || id_vehicle == undefined) {
		createAlert('Debes seleccionar un vehículo');
		add = false;
		return 0;	
	}

	if(add) {
		var routes = [];
		$.each($('div#routes_container div.card'), function(i, elem) {
			var id = $(elem).attr('id').replace('route_','');

			var id_client = $('select#id_client_' + id).val();
			var id_location = $('select#id_location_' + id).val();
			var packages = $('input#packages_' + id).val();
			var status = $('select#id_status_' + id).val();

			if(id_client == 0 || id_client == undefined) {
				createAlert('Debes seleccionar un cliente válido.');
				$('select#id_client_' + id).addClass('border-danger');
				add = false;
				return 0;
			}

			if(id_location == 0 || id_location == undefined) {
				createAlert('Debes seleccionar una ubicación válida.');
				$('select#id_location_' + id).addClass('border-danger');
				add = false;
				return 0;
			}

			if(packages < 1) {
				createAlert('Debes introducir una cantidad de paquetes válida.');
				$('input#packages_' + id).addClass('border-danger');
				add = false;
				return 0;	
			}

			var route = {};
			route.id_client = id_client;
			route.id_location = id_location;
			route.packages = packages;
			route.id_route_status = status;
			route.score = 0;

			routes.push(route);
		});

		if(routes.length > 0 && add) {
			var data = new FormData;
			data.append('id_driver', id_driver);
			data.append('id_vehicle', id_vehicle);
			data.append('date', date);
			data.append('id_route_status', id_route_status);
			data.append('routes', JSON.stringify(routes));

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('input[name="_token"]').val()
				},
			    method: 'POST',
			    url: url + 'rutas',
			    data: data,
			    dataType:'json',
			    processData: false,
				contentType: false,
				success:function(response){	
					if(response.message) {
						createAlert(response.message);
						
						$('.alertWindowOk').on('click', function() {
							if(response.redirect) {
								document.location = response.redirect;
							}
						})
					}
				},
			});
		} else {
			createAlert('No has agregado ninguna ruta.');
		}
	}
}