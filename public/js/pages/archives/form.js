$(document).ready(function() {
	$('select#client_id').on('change', showDropzone);
	Dropzone.options.archivesDropzone = {
	    maxFiles: 100,
	    acceptedFiles : '.pdf, .xml, .PDF, .XML',
	    dictDefaultMessage : 'Arrasta los archivos aquí que quieras subir al servidor'
	  };
});

var showDropzone = function() {
	var client_id = $(this).val();
	
	if(client_id == 0) {
		$('form#archives-dropzone').fadeOut();
	} else {
		$('form#archives-dropzone').fadeIn();
		$('input#dropzone_client_id').val(client_id);
	}
}