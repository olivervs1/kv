$(document).ready(function() {
	$('button#download').on('click', downloadFiles);
	$('button#search').on('click', searchFiles);
});

var downloadFiles = function() {
	var add = true;
	var from = $('input#from').val();
	var to = $('input#to').val();

	if(from == '' || from == undefined) {
		$('input#from').addClass('border-danger');
		add = false;
	}

	if(to == '' || to == undefined) {
		$('input#to').addClass('border-danger');
		add = false;
	}

	if(add) {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: $('form#files_form').attr('action'),
		    data: new FormData($('form#files_form')[0]),
		    dataType:'json',
		    processData: false,
			contentType: false,
			beforeSend: function() {
				$('button#download').prop('disabled', 'disabled');
				$('button#download').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
			},
			success:function(response){	
				$('button#download').prop('disabled', '');
				$('button#download').html('Descargar <i class="fas fa-file-archive ml-2"></i>');

				if(response.message) {
					createAlert(response.message);
				}

				if(response.file) {
					window.open(response.file);
				}

				$('div.modal-backdrop').modal('hide');
			},
			error: function(response) {
				$('button#download').prop('disabled', '');
				$('button#download').html('Descargar <i class="fas fa-file-archive ml-2"></i>');
			}
		});
	}
}

var searchFiles = function() {
	var from = $('input#search_from').val();
	var to = $('input#search_to').val();

	if(from != "" && to != "") {
		if(from <= to) {
			var url = $('input#url').val();
			document.location = url + '?from=' + from + '&to=' + to;
		} else {
			// La fecha de inicio es mayor que la de fin
		}
	}
}