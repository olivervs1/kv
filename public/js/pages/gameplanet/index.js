$(document).ready(function() {
	$('button#download-remission').on('click', getRemission);
	$('input.type_selection').on('change', showSelection);
});

var getRemission = function() {
	$('input#remission_number').removeClass('border-danger');
	$('select#route_number').removeClass('border-danger');
	$('div#get_new_remission span.error').fadeOut();

	var remission_number = $('input#remission_number').val();
	var route_number = $('select#route_number').val();
	var store_number = $('select#store_number').val();
	var region_type = $('select#region_type').val();
	var option_selected = $('input[name=get_by]:checked').val();
	var add = true;

	if(remission_number == "" || remission_number == undefined || remission_number == null) {
		$('input#remission_number').addClass('border-danger');
		add = false;
	}

	if(option_selected == "route") {
		if(route_number == "" || route_number == undefined || route_number == null) {
			$('select#route_number').addClass('border-danger');
			add = false;
		}	
	}

	if(option_selected == "region") {
		if(region_type == "" || region_type == undefined || region_type == null) {
			$('select#region_type').addClass('border-danger');
			add = false;
		}	
	}

	if(option_selected == "store") {
		if(store_number == "" || store_number == undefined || store_number == null) {
			$('select#store_number').addClass('border-danger');
			add = false;
		}	
	}
	

	if(add) {
		console.log('entro');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('input[name="_token"]').val()
			},
		    method: 'POST',
		    url: $('form#remission_form').attr('action'),
		    data: new FormData($('form#remission_form')[0]),
		    dataType:'json',
		    processData: false,
			contentType: false,
			beforeSend: function() {
				$('button#download-remission').prop('disabled', 'disabled');
				$('button#download-remission').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
			},
			success:function(response){	
				if(response.message) {
					createAlert(response.message);
				}

				if(response.file) {
					window.open(response.file);
				}
			},
			error: function(response) {
				$('div#get_new_remission span.error').html('No hay información para ese número de distribución y tienda destino.');
				$('div#get_new_remission span.error').fadeIn();
			}
		});
	}
}

var showSelection = function() {
	var val = $(this).val();

	if(val == "region") {
		$('div#region_selection').slideDown();
		$('div#store_selection').slideDown();
		if($('div#route_selection').is(':visible')) {
			$('div#route_selection').slideUp();
		}

		if($('div#store_selection').is(':visible')) {
			$('div#store_selection').slideUp();
		}
	} else if(val == "route"){
		$('div#route_selection').slideDown();
		$('div#store_selection').slideDown();

		if($('div#region_selection').is(':visible')) {
			$('div#region_selection').slideUp();
		}

		if($('div#store_selection').is(':visible')) {
			$('div#store_selection').slideUp();
		}
	} else if(val == "store"){
		$('div#store_selection').slideDown();
		if($('div#region_selection').is(':visible')) {
			$('div#region_selection').slideUp();
		}

		if($('div#route_selection').is(':visible')) {
			$('div#route_selection').slideUp();
		}
	}
}