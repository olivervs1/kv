$(document).ready(function() {
	$('#submit').on('click', submitForm);
	$('select.autocomplete').on('change', autocompleteData);
});

var submitForm = function(e) {
	e.preventDefault();
	
	var add = true;

	$.each($('input.required'), function(i, e) {
		var $e = $(e);

		if($e.val() == "" || $e.val().length == 0 || $(e).val() == undefined) {
			add = false;
			$(e).addClass('border').addClass('border-danger');
		}
	})

	$.each($('select.required-select'), function(i, e) {
		var $e = $(e);

		if($e.val() == "0" || $e.val() == 0) {
			add = false;
			$(e).addClass('border').addClass('border-danger');
		}
	});

	if(add) {
		$.ajax({
		    headers: {
		        'X-CSRF-TOKEN': $('input[name="_token"]').val()
		    },
		    method: 'POST',
		    url: $('form#form').attr('action'),
		    data: new FormData($('form#form')[0]),
		    dataType:'json',
		    processData: false,
		    contentType: false,
		    success:function(response){                 
		        
		        if(response.message) {
		            createAlert(response.message);

		            $('.alertWindowOk').on('click', function() {
		                if(response.status) {
		                	if(response.redirect) {
		                		document.location = response.redirect;	
		                	}
		                }
		            });
		        }
		    },
		});
	}
}

var autocompleteData = function() {
	var id = $(this).val();
	var target = $(this).data('target');
	var link = $(this).data('link');

	if(link == "" || target == "") {
		return 4;
	}

	if(id != 0) {
		var formData = new FormData();
		formData.append('id', id);

		$.ajax({
		    method: 'POST',
		    url: link,
		    data: formData,
		    dataType:'json',
		    processData: false,
		    contentType: false,
		    success:function(response){                 
		        if(response.status) {
		        	var html = "";
		        	if(response.data.length > 0) {
		        		$.each(response.data, function(i, elem) {
		        			html += '<option value="' + elem.id + '">' + elem.name + '</option>';
		        		});	
		        	} else {
		        		html += '<option value="0">No hay datos para mostrar</option>';
		        	}
		        	
		        	$('select#' + target).html(html);
		        } else {
		        	var html = '<option value="0">No hay datos para mostrar</option>';
		        	$('select#' + target).html(html);
		        }
		    },
		});	
	}
}