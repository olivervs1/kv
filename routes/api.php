<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);


Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('logout', [ApiController::class, 'logout']);
    Route::post('get_user', [ApiController::class, 'get_user']);

    Route::post('getRoutes', [ApiController::class, 'getRoutes']);
    Route::post('start', [ApiController::class, 'startRoute']);
    Route::post('end', [ApiController::class, 'endRoute']);
    Route::post('finishRoute', [ApiController::class, 'finishRoute']);
    Route::post('getRouteDetail', [ApiController::class, 'getRouteDetail']);

    Route::post('sendMessage', [ApiController::class, 'sendMessage']);
    Route::post('getMessages', [ApiController::class, 'getMessages']);
    Route::post('getChatUsers', [ApiController::class, 'getChatUsers']);
    Route::post('sos', [ApiController::class, 'sos']);
});
   
Route::post('updateRoute', [ApiController::class, 'updateRoute']);
Route::post('cancelRoute', [ApiController::class, 'cancelRoute']);
Route::post('getSurvey', [ApiController::class, 'getSurvey']);
Route::post('getCancelMotives', [ApiController::class, 'getCancelMotives']);
Route::get('clear', [ApiController::class, 'cleanData']);

Route::get('getRouteDetail/{id}', [ApiController::class, 'getRouteDetail']);

Route::get('getMessagesTest', [ApiController::class, 'getMessagesTest']);