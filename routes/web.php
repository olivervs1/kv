<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', [App\Http\Controllers\HomeController::class, 'test']);

Route::middleware(['admin'])->group(function() {
    Route::get('/home/{date?}', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    ### CLIENTES ###
    Route::get('/clientes', [App\Http\Controllers\ClientsController::class, 'index']);
    Route::get('/clientes/agregar', [App\Http\Controllers\ClientsController::class, 'create']);
    Route::post('/clientes/ubicaciones',[App\Http\Controllers\ClientsController::class, 'getLocations']);
    Route::get('/clientes/{id}', [App\Http\Controllers\ClientsController::class, 'show']);
    Route::get('/clientes/{id}/editar',[App\Http\Controllers\ClientsController::class, 'edit']);
    Route::post('/clientes',[App\Http\Controllers\ClientsController::class, 'store']);
    Route::post('/clientes/{id}',[App\Http\Controllers\ClientsController::class, 'update']);

    ### UBICACIONES ###
    Route::get('/ubicaciones', [App\Http\Controllers\LocationsController::class, 'index']);
    Route::get('/ubicaciones/agregar', [App\Http\Controllers\LocationsController::class, 'create']);
    Route::get('/ubicaciones/{id}', [App\Http\Controllers\LocationsController::class, 'show']);
    Route::get('/ubicaciones/{id}/editar',[App\Http\Controllers\LocationsController::class, 'edit']);
    Route::post('/ubicaciones',[App\Http\Controllers\LocationsController::class, 'store']);
    Route::post('/ubicaciones/{id}',[App\Http\Controllers\LocationsController::class, 'update']);

    ### RUTAS ###
    Route::get('/rutas', [App\Http\Controllers\RouteController::class, 'index']);
    Route::get('/rutas/agregar', [App\Http\Controllers\RouteController::class, 'create']);
    Route::get('/rutas/{id}', [App\Http\Controllers\RouteController::class, 'show']);
    Route::get('/rutas/{id}/editar',[App\Http\Controllers\RouteController::class, 'edit']);
    Route::post('/rutas',[App\Http\Controllers\RouteController::class, 'store']);
    Route::post('/rutas/{id}',[App\Http\Controllers\RouteController::class, 'update']);

    ### RUTAS PRECONFIGURADAS ###
    Route::get('/rutas-preconfiguradas', [App\Http\Controllers\PreconfiguredRouteController::class, 'index']);
    Route::get('/rutas-preconfiguradas/agregar', [App\Http\Controllers\PreconfiguredRouteController::class, 'create']);
    Route::get('/rutas-preconfiguradas/{id}', [App\Http\Controllers\PreconfiguredRouteController::class, 'show']);
    Route::get('/rutas-preconfiguradas/{id}/editar',[App\Http\Controllers\PreconfiguredRouteController::class, 'edit']);
    Route::post('/rutas-preconfiguradas',[App\Http\Controllers\PreconfiguredRouteController::class, 'store']);
    Route::post('/rutas-preconfiguradas/{id}',[App\Http\Controllers\PreconfiguredRouteController::class, 'update']);

    ### TIPOS DE RUTAS ###
    Route::get('/tipos-de-rutas', [App\Http\Controllers\RouteTypeController::class, 'index']);
    Route::get('/tipos-de-rutas/agregar', [App\Http\Controllers\RouteTypeController::class, 'create']);
    Route::get('/tipos-de-rutas/{id}', [App\Http\Controllers\RouteTypeController::class, 'show']);
    Route::get('/tipos-de-rutas/{id}/editar',[App\Http\Controllers\RouteTypeController::class, 'edit']);
    Route::post('/tipos-de-rutas',[App\Http\Controllers\RouteTypeController::class, 'store']);
    Route::post('/tipos-de-rutas/{id}',[App\Http\Controllers\RouteTypeController::class, 'update']);

    ### CHOFERES ###
    Route::get('/choferes', [App\Http\Controllers\DriversController::class, 'index']);
    Route::get('/choferes/test', [App\Http\Controllers\DriversController::class, 'test']);
    Route::get('/choferes/agregar', [App\Http\Controllers\DriversController::class, 'create']);
    Route::get('/choferes/{id}', [App\Http\Controllers\DriversController::class, 'show']);
    Route::get('/choferes/{id}/editar',[App\Http\Controllers\DriversController::class, 'edit']);
    Route::post('/choferes',[App\Http\Controllers\DriversController::class, 'store']);
    Route::post('/choferes/{id}',[App\Http\Controllers\DriversController::class, 'update']);

    ### CHOFERES: CHAT ###
    Route::get('/choferes/{id}/chat', [App\Http\Controllers\DriversController::class, 'chat']);
    Route::post('/choferes/{id}/chat/enviar-mensaje', [App\Http\Controllers\DriversController::class, 'sendMessage']);

    ### VEHÍCULOS ###
    Route::get('/vehiculos', [App\Http\Controllers\VehiclesController::class, 'index']);
    Route::get('/vehiculos/agregar', [App\Http\Controllers\VehiclesController::class, 'create']);
    Route::get('/vehiculos/{id}', [App\Http\Controllers\VehiclesController::class, 'show']);
    Route::get('/vehiculos/{id}/editar',[App\Http\Controllers\VehiclesController::class, 'edit']);
    Route::post('/vehiculos',[App\Http\Controllers\VehiclesController::class, 'store']);
    Route::post('/vehiculos/{id}',[App\Http\Controllers\VehiclesController::class, 'update']);

    ### VEHÍCULOS ###
    Route::get('/estatus-rutas', [App\Http\Controllers\RouteStatusController::class, 'index']);
    Route::get('/estatus-rutas/agregar', [App\Http\Controllers\RouteStatusController::class, 'create']);
    Route::get('/estatus-rutas/{id}', [App\Http\Controllers\RouteStatusController::class, 'show']);
    Route::get('/estatus-rutas/{id}/editar',[App\Http\Controllers\RouteStatusController::class, 'edit']);
    Route::post('/estatus-rutas',[App\Http\Controllers\RouteStatusController::class, 'store']);
    Route::post('/estatus-rutas/{id}',[App\Http\Controllers\RouteStatusController::class, 'update']);

    ### ENCUESTA ###
    Route::get('/encuesta', [App\Http\Controllers\SurveyController::class, 'index']);
    Route::get('/encuesta/agregar', [App\Http\Controllers\SurveyController::class, 'create']);
    Route::get('/encuesta/{id}', [App\Http\Controllers\SurveyController::class, 'show']);
    Route::get('/encuesta/{id}/editar',[App\Http\Controllers\SurveyController::class, 'edit']);
    Route::post('/encuesta',[App\Http\Controllers\SurveyController::class, 'store']);
    Route::post('/encuesta/{id}',[App\Http\Controllers\SurveyController::class, 'update']);

    ### GAMEPLANET ###
    Route::post('/gameplanet/nueva-remision', [App\Http\Controllers\GameplanetController::class, 'newRemission']);
    Route::get('/gameplanet/test', [App\Http\Controllers\GameplanetController::class, 'test']);
    Route::get('/gameplanet', [App\Http\Controllers\GameplanetController::class, 'index']);
    Route::get('/gameplanet/agregar', [App\Http\Controllers\GameplanetController::class, 'create']);
    Route::get('/gameplanet/{id}', [App\Http\Controllers\GameplanetController::class, 'show']);
    Route::get('/gameplanet/{id}/editar',[App\Http\Controllers\GameplanetController::class, 'edit']);
    Route::post('/gameplanet',[App\Http\Controllers\GameplanetController::class, 'store']);
    Route::post('/gameplanet/{id}',[App\Http\Controllers\GameplanetController::class, 'update']);

    ### MOTIVOS DE CANCELACIÓN ###
    Route::get('/motivos-de-cancelacion', [App\Http\Controllers\CancelMotivesController::class, 'index']);
    Route::get('/motivos-de-cancelacion/agregar', [App\Http\Controllers\CancelMotivesController::class, 'create']);
    Route::get('/motivos-de-cancelacion/{id}', [App\Http\Controllers\CancelMotivesController::class, 'show']);
    Route::get('/motivos-de-cancelacion/{id}/editar',[App\Http\Controllers\CancelMotivesController::class, 'edit']);
    Route::post('/motivos-de-cancelacion',[App\Http\Controllers\CancelMotivesController::class, 'store']);
    Route::post('/motivos-de-cancelacion/{id}',[App\Http\Controllers\CancelMotivesController::class, 'update']);

    ### ARCHIVO: CARTA PORTE ###
    Route::get('/carta-porte', [App\Http\Controllers\ArchiveController::class, 'index']);
    Route::get('/carta-porte/agregar', [App\Http\Controllers\ArchiveController::class, 'create']);
    Route::post('/carta-porte/ubicaciones',[App\Http\Controllers\ArchiveController::class, 'getLocations']);
    Route::get('/carta-porte/{id}', [App\Http\Controllers\ArchiveController::class, 'show']);
    Route::get('/carta-porte/{id}/editar',[App\Http\Controllers\ArchiveController::class, 'edit']);
    Route::get('/carta-porte/{id}/eliminar', [App\Http\Controllers\ArchiveController::class, 'destroy']);
    Route::post('/carta-porte',[App\Http\Controllers\ArchiveController::class, 'store']);
    Route::post('/carta-porte/descargar',[App\Http\Controllers\ArchiveController::class, 'download']);
    Route::post('/carta-porte/{id}',[App\Http\Controllers\ArchiveController::class, 'update']);
});
