<?php

namespace App\Http\Controllers;

use App\Jobs\DownloadRemissions;
use App\Models\Locations;
use App\Models\RouteType;
use App\Models\PreconfiguredRoute;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

use File;
use Mail;
use ZipArchive;
class GameplanetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $files = array();
        $routes = PreconfiguredRoute::all();
        $routeTypes = RouteType::all();

        return view('pages.gameplanet.gameplanet-list',compact('files', 'routes', 'routeTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newRemission(Request $request) {
        $remission_number = $request->remission_number;
        $route_number = $request->route_number;
        $store_number = $request->store_number;
        $get_by = $request->get_by;
        $region_type = $request->region_type;

        if($get_by == "store" && $store_number == 0) {
            DownloadRemissions::dispatch($remission_number);

            return response()->json(['status' => true, 'file' => url('/exports/remisiones.zip')]);
        }

        $routes = array();

        if($get_by == "route") {
            $route = PreconfiguredRoute::find($route_number);
            
            if(!$route instanceof PreconfiguredRoute) {
                return response()->json(['status' => false, 'message' => 'La ruta seleccionada no es válida.']);
            }

            if($route->Locations->count() == 0) {
                return response()->json(['status' => false, 'message' => 'La ruta seleccionada no tiene ubicaciones asignadas.']);
            }

            $routes[] = $route->Locations;
        } elseif($get_by == "region") {
            $preconfiguredRoutes = PreconfiguredRoute::where('route_type_id', '=', $region_type)->get();

            foreach ($preconfiguredRoutes as $r) {
                array_push($routes, $r->Locations);
            }
        } elseif($get_by == "store") {
            $store = Locations::find($store_number);
            $routes[][] = $store;
        }

        ///// CODIGO PARA GAMEPLANET /////
        $endpoint = "https://epod4.gameplanet.com/bridge/logistica/carta_porte/mercancia";
        $client = new \GuzzleHttp\Client();
        //$distribucion = '20211030-211039';
        //$destino = "30";

        $distribucion = $remission_number;
        $remissions = explode(',', $remission_number);

        $html = '<table>';
        $html .= '<tr>';
        $html .= '<td>Cantidad</td>';
        $html .= '<td>ID unidad embalaje</td>';
        $html .= utf8_decode('<td>Descripción material carga</td>');
        $html .= '<td>Peso</td>';
        $html .= '<td>ID unidad de peso</td>';
        $html .= '<td>Clave Productos y servicios</td>';
        $html .= '<td>Clave Unidad</td>';
        $html .= '<td>ID Tienda Origen</td>';
        $html .= '<td>Clave Tienda Origen</td>';
        $html .= '<td>ID Tienda Destino</td>';
        $html .= '<td>Clave Tienda Destino</td>';
        $html .= '<td>ID Ruta</td>';
        $html .= '<td>Ruta</td>';
        $html .= '<td>Zona</td>';
        $html .= '<td>Remision</td>';
        $html .= '</tr>';

        foreach ($remissions as $remission) {
            foreach ($routes as $data) {
                foreach ($data as $loc) {
                    $destino = 0;
                    if($loc instanceof Locations) {
                        $destino = $loc->store_id;
                    } else {
                        $destino = $loc->Location->store_id;
                    }

                    try {
                        $response = $client->request('GET', $endpoint, ['query' => [
                            'distribucion' => trim($remission),
                            'destino' => $destino
                            ],
                            'auth' => [
                                'gp_externo',
                                'tAsN4eWRSG3ByM'
                            ],
                            'http_errors' => false
                        ]);

                        $statusCode = $response->getStatusCode();
                        
                        if($statusCode != 200) {
                            continue;
                        }
                        
                        $content = $response->getBody();

                        // or when your server returns json
                        $content = json_decode($response->getBody(), true);
                    } catch (RequestException $e) {
                        var_dump($e); exit;
                        // Catch all 4XX errors 
                        
                        // To catch exactly error 400 use 
                        if ($e->hasResponse()){
                            if ($e->getResponse()->getStatusCode() == '400') {
                                    var_dump("Got response 400"); exit;
                            }
                        }

                        // You can check for whatever error status code you need 
    
                    } catch (Exception $e) {
                        var_dump($e); exit;
                    }

                    if(isset($content['success'])) {
                        if(isset($content['list']['status']) && $content['list']['status'] == "success") {
                            if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
                                mkdir(env('FILESYSTEM') . 'public/exports/');
                            }

                            foreach($content['list']['list'] as $p) {
                                $originStore = Locations::where('store_id','=', $p['origen'])->first();
                                
                                $html .= '<tr>';
                                    $html .= '<td>' . $p['cantidad'] . '</td>';
                                    $html .= '<td>' . $p['unidad_embalaje'] . '</td>';
                                    $html .= '<td>' . $p['descripcion'] . '</td>';
                                    $html .= '<td>' . $p['peso'] . '</td>';
                                    $html .= '<td>' . $p['unidad_peso'] . '</td>';
                                    $html .= '<td>' . $p['clave_productos_servicios'] . '</td>';
                                    $html .= '<td>' . $p['clave_unidad'] . '</td>';
                                    $html .= '<td>' . $p['origen'] . '</td>';
                                    if($originStore != NULL) {
                                        $html .= '<td>' . $originStore->store_key . '</td>';
                                    } else {
                                        $html .= '<td></td>';
                                    }
                                    
                                    $html .= '<td>' . $destino . '</td>';
                                    $html .= '<td>' . $p['id_ruta'] . '</td>';
                                    $html .= '<td>' . $p['ruta'] . '</td>';
                                    $html .= '<td>' . $p['zona'] . '</td>';
                                    $html .= '<td>' . $p['clave'] . '</td>';
                                    $html .= '<td>' . $remission . '</td>';
                                $html .= '</tr>';
                            }
                        }
                    }
                }
            }   
        }

        $html .= '</table>';

        $filename = 'remisiones.xls';

        file_put_contents(env('FILESYSTEM') . '/public/exports/' . $filename, $html);
        return response()->json(['status' => true, 'file' => url('/exports/' . $filename)]);
    }

    public function test() {
        ///// CODIGO PARA GAMEPLANET /////
        $endpoint = "https://epod4.gameplanet.com/bridge/logistica/carta_porte/mercancia";
        $client = new \GuzzleHttp\Client();
        $distribucion = '20220127-160116';
        $destino = "254";

        $response = $client->request('GET', $endpoint, ['query' => [
            'distribucion' => trim($distribucion),
            'destino' => $destino
            ],
            'auth' => [
                'gp_externo',
                'tAsN4eWRSG3ByM'
            ],
            'http_errors' => false
        ]);

        $statusCode = $response->getStatusCode();

        if($statusCode != 200) {
            //continue;
        }
        
        $content = $response->getBody();

        // or when your server returns json
        $content = json_decode($response->getBody(), true);

        $remissions = explode(',', '20220127-160116');

        $routes = PreconfiguredRoute::all();
        $files = array();
        
        foreach ($routes as $route) {
            foreach ($route->Locations as $loc) {
                $html = '<table>';
                $html .= '<tr>';
                $html .= '<td>Cantidad</td>';
                $html .= '<td>ID unidad embalaje</td>';
                $html .= utf8_decode('<td>Descripción material carga</td>');
                $html .= '<td>Peso</td>';
                $html .= '<td>ID unidad de peso</td>';
                $html .= '<td>Clave Productos y servicios</td>';
                $html .= '<td>Clave Unidad</td>';
                $html .= '<td>ID Tienda Origen</td>';
                $html .= '<td>ID Tienda Destino</td>';
                $html .= '<td>ID Ruta</td>';
                $html .= '<td>Ruta</td>';
                $html .= '<td>Zona</td>';
                $html .= '<td>Clave</td>';
                $html .= '<td>Remision</td>';
                $html .= '</tr>';
                foreach ($remissions as $remission) {
                    $destino = 0;
                    if($loc instanceof Locations) {
                        $destino = $loc->store_id;
                    } else {
                        $destino = $loc->Location->store_id;
                    }

                    try {
                        $response = $client->request('GET', $endpoint, ['query' => [
                            'distribucion' => trim($remission),
                            'destino' => $destino
                            ],
                            'auth' => [
                                'gp_externo',
                                'tAsN4eWRSG3ByM'
                            ],
                            'http_errors' => false
                        ]);

                        $statusCode = $response->getStatusCode();
                        
                        if($statusCode != 200) {
                            continue;
                        }
                        
                        $content = $response->getBody();

                        // or when your server returns json
                        $content = json_decode($response->getBody(), true);
                    } catch (RequestException $e) {
                        var_dump($e); exit;
                        // Catch all 4XX errors 
                        
                        // To catch exactly error 400 use 
                        if ($e->hasResponse()){
                            if ($e->getResponse()->getStatusCode() == '400') {
                                    var_dump("Got response 400"); exit;
                            }
                        }

                        // You can check for whatever error status code you need 
        
                    } catch (Exception $e) {
                        var_dump($e); exit;
                    }

                    if(isset($content['success'])) {
                        if(isset($content['list']['status']) && $content['list']['status'] == "success") {
                            if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
                                mkdir(env('FILESYSTEM') . 'public/exports/');
                            }

                            if(!is_dir(env('FILESYSTEM') . 'public/exports/stores/')) {
                                mkdir(env('FILESYSTEM') . 'public/exports/stores/');
                            }

                            foreach($content['list']['list'] as $p) {
                                $html .= '<tr>';
                                    $html .= '<td>' . $p['cantidad'] . '</td>';
                                    $html .= '<td>' . $p['unidad_embalaje'] . '</td>';
                                    $html .= '<td>' . $p['descripcion'] . '</td>';
                                    $html .= '<td>' . $p['peso'] . '</td>';
                                    $html .= '<td>' . $p['unidad_peso'] . '</td>';
                                    $html .= '<td>' . $p['clave_productos_servicios'] . '</td>';
                                    $html .= '<td>' . $p['clave_unidad'] . '</td>';
                                    $html .= '<td>' . $p['origen'] . '</td>';
                                    $html .= '<td>' . $destino . '</td>';
                                    $html .= '<td>' . $p['id_ruta'] . '</td>';
                                    $html .= '<td>' . $p['ruta'] . '</td>';
                                    $html .= '<td>' . $p['zona'] . '</td>';
                                    $html .= '<td>' . $p['clave'] . '</td>';
                                    $html .= '<td>' . $remission . '</td>';
                                $html .= '</tr>';
                            }
                        }
                    }
                }

                $html .= '</table>';

                $filename = $destino . '.xls';

                file_put_contents(env('FILESYSTEM') . '/public/exports/stores/' . $filename, $html);

                $files[] = env('FILESYSTEM') . '/public/exports/stores/' . $filename;
            }   
        }

        $zip = new \ZipArchive();
        $fileName = 'remisiones.zip';
        if ($zip->open(env('FILESYSTEM') . '/public/exports/remisiones.zip', \ZipArchive::CREATE)== TRUE)
        {
            $files = File::files(env('FILESYSTEM') . '/public/exports/stores/');
            foreach ($files as $key => $value){
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }

        $subscribers = array();
        $subscribers[] = array('email' => 'olivervs@hotmail.es', 'name' => 'Oliver Vilchiz');

        foreach ($subscribers as $subscriber) {
            Mail::send('emails.remissions_ready', ['uri' => url('/exports/remisiones.zip')], function ($m) use($subscriber) {
                $m->to($subscriber['email'], $subscriber['name']);
                $m->subject('El archivo esta listo para ser descargado.');
            });
        }
    }
}
