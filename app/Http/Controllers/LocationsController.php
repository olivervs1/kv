<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\Locations;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function index() {
    	$locations = Locations::get();
    	return view('pages.locations.locations-list',compact('locations'));
    }

	public function create() {
        $clients = Clients::orderby('comercial_name')->get();
		return view('pages.locations.locations-form', compact('clients'));
    }    

    public function edit(Request $request) {
        $clients = Clients::orderby('comercial_name')->get();
    	$location = Locations::find($request->id);

    	if($location instanceof Locations ) {
    		return view('pages.locations.locations-form', compact('location', 'clients'));
    	} 

    	return redirect('/ubicaciones');
    }

    public function show(Request $request) {
    	$location = Locations::find($request->id);

    	if($location instanceof Locations ) {
    		return view('pages.locations.locations-record', compact('location'));
    	} 

    	return redirect('/ubicaciones');
    }

    public function store(Request $request) {
    	$location = new Locations;
    	$location->id_client = $request->id_client;
        $location->store_id = $request->store_id;
        $location->store_key = $request->store_key;
    	$location->name = $request->name;
    	$location->street = $request->street;
    	$location->ext_number = $request->ext_number;
    	$location->int_number = $request->int_number;
    	$location->colony = $request->colony;
    	$location->zipcode = $request->zipcode;
    	$location->city = $request->city;
    	$location->state = $request->state;
        $location->lat = $request->latitude;
        $location->long = $request->longitude;

    	try {
    		if($location->save()) {
    			echo json_encode(array('status' => true, 'message' => 'La ubicación ha sido agregada correctamente.', 'redirect' => url('/ubicaciones/' . $location->id)));
    			exit;
    		}	
    	} catch (Exception $e) {
    		echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    		exit;
    	}
    }

    public function update(Request $request) {
    	$location = Locations::find($request->id);

    	if($location instanceof Locations) {
    		$location->id_client = $request->id_client;
            $location->store_id = $request->store_id;
            $location->store_key = $request->store_key;
	    	$location->name = $request->name;
	    	$location->street = $request->street;
	    	$location->ext_number = $request->ext_number;
	    	$location->int_number = $request->int_number;
	    	$location->colony = $request->colony;
	    	$location->zipcode = $request->zipcode;
	    	$location->city = $request->city;
	    	$location->state = $request->state;
            $location->lat = $request->latitude;
            $location->long = $request->longitude;

    		try {
    			if($location->save()) {
    				echo json_encode(array('status' => true, 'message' => 'La uicación ha sido editada correctamente.', 'redirect' => url('/ubicaciones/' . $location->id)));
    				exit;
    			}	
    		} catch (Exception $e) {
    			echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    			exit;
    		}
    	}
    }
}
