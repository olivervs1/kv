<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\Archive;
use Illuminate\Http\Request;

use File;
use ZipArchive;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $date = date('Y') . '-' . date('m');

        if(isset($request->from) && isset($request->to)) {
            $archives = Archive::whereDate('date', '>=', $request->from)->whereDate('date','<=', $request->to)->get();
        } else {
            $archives = Archive::where('date','LIKE',"%" . $date . "%")->get();
        }
        
        $clients = Clients::orderBy('comercial_name')->get();

        return view('pages.archives.archives-list', compact('archives', 'clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $clients = Clients::orderBy('comercial_name')->get();

        return view('pages.archives.archives-form', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $file = $request->file;
        $client_id = $request->client_id;

        $imageInfo = pathinfo($file->getClientOriginalName());
        $filename = $imageInfo['filename'];
        $name = str_replace(['pdf',' pdf'],['',''], $imageInfo['filename']) . '.' . $imageInfo['extension'];

        if(!is_dir(env('FILESYSTEM') . '/public/archives/')) {
            mkdir(env('FILESYSTEM') . '/public/archives/');
        }

        if(move_uploaded_file($file->getPathName(), env('FILESYSTEM') . 'public/archives/' . $name)) {

            $exist = "";

            if(strtolower($imageInfo['extension']) == 'pdf') { 
                $exist = Archive::where('pdf','=', $name)->first();
            } else {
                $exist = Archive::where('xml','=', $name)->first();
            }

            if($exist instanceof Archive) {
                return response()->json(['status' => true]);
            }

            $date = "";
            $subtotal = "";
            $total = "";

            if(strtolower($imageInfo['extension']) == 'pdf') {
                $fileObj = Archive::where('xml','=', str_replace('.pdf', '.xml', $name))->first();

                if($fileObj instanceof Archive) {
                    $fileObj->pdf = $name;
                    $fileObj->client_id = $client_id;
                    $fileObj->save();
                } else {
                    $fileObj = new Archive;
                    $fileObj->pdf = $name;
                    $fileObj->client_id = $client_id;
                    $fileObj->save();
                }
            }

            if(strtolower($imageInfo['extension'] == 'xml')) {
                $fileObj = Archive::where('pdf','=', str_replace('.xml', '.pdf', $name))->first();   

                $xml = simplexml_load_file(env('FILESYSTEM') . 'public/archives/' . $name);
                $date = str_replace('T', ' ', $xml['Fecha']);
                $subtotal = $xml['SubTotal'];
                $total = $xml['Total'];

                $notes = array('subtotal' => $subtotal, 'total' => $total);

                if($fileObj instanceof Archive) {
                    $fileObj->xml = $name;
                    $fileObj->date = $date;
                    $fileObj->notes = $notes;
                    $fileObj->client_id = $client_id;
                    $fileObj->save();
                } else {
                    $fileObj = new Archive;
                    $fileObj->xml = $name;
                    $fileObj->date = $date;
                    $fileObj->notes = $notes;
                    $fileObj->client_id = $client_id;
                    $fileObj->save();
                }
            }            

            return response()->json(['status' => true]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Archive $id) {
        $archive = $id;
        $clients = Clients::orderBy('comercial_name')->get();
        return view('pages.archives.archives-edit', compact('archive', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Archive $id) {
        $archive = $id;
        $client_id = $request->client_id;

        if(isset($request->pdf)) {
            $file = $request->pdf;
            $imageInfo = pathinfo($file->getClientOriginalName());
            $filename = $imageInfo['filename'];
            $name = str_replace(['pdf',' pdf'],['',''], $imageInfo['filename']) . '.' . $imageInfo['extension'];

            if(!is_dir(env('FILESYSTEM') . '/public/archives/')) {
                mkdir(env('FILESYSTEM') . '/public/archives/');
            }

            if(move_uploaded_file($file->getPathName(), env('FILESYSTEM') . 'public/archives/' . $name)) {
                $archive->pdf = $name;
                $archive->client_id = $client_id;
                $archive->save();
            }
        }

        if(isset($request->xml)) {
            $file = $request->xml;
            $imageInfo = pathinfo($file->getClientOriginalName());
            $filename = $imageInfo['filename'];
            $name = str_replace(['pdf',' pdf'],['',''], $imageInfo['filename']) . '.' . $imageInfo['extension'];

            if(!is_dir(env('FILESYSTEM') . '/public/archives/')) {
                mkdir(env('FILESYSTEM') . '/public/archives/');
            }

            if(move_uploaded_file($file->getPathName(), env('FILESYSTEM') . 'public/archives/' . $name)) {
                $xml = simplexml_load_file(env('FILESYSTEM') . 'public/archives/' . $name);
                $date = str_replace('T', ' ', $xml['Fecha']);
                $subtotal = $xml['SubTotal'];
                $total = $xml['Total'];

                $notes = array('subtotal' => $subtotal, 'total' => $total);

                $archive->xml = $name;
                $archive->date = $date;
                $archive->notes = $notes;
                $archive->client_id = $client_id;
                $archive->save();
            }
        }

        $archive->client_id = $client_id;
        $archive->save();
        
        return redirect('/carta-porte');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Archive $id) {
        $file = $id;

        if($file->pdf != NULL) {
            if(is_file(env('FILESYSTEM') . 'public/archives/' . $file->pdf)) {
                unlink(env('FILESYSTEM') . 'public/archives/' . $file->pdf);
            }            
        }

        if($file->xml != NULL) {
            if(is_file(env('FILESYSTEM') . 'public/archives/' . $file->xml)) {
                unlink(env('FILESYSTEM') . 'public/archives/' . $file->xml);
            }            
        }

        if($file->delete()) {
            return redirect('/carta-porte');
        }
    }

    public function download(Request $request) {
        $from = $request->from;
        $to = $request->to;
        $client_id = $request->client_id;

        $files = Archive::whereBetween('date',[$from, $to])->where('client_id','=', $client_id)->get();

        $zip = new \ZipArchive();
        $fileName = 'cartas-porte.zip';

        if(is_file(env('FILESYSTEM') . '/public/archives/archives_' . date('d-m-Y') . '.zip')) {
            unlink(env('FILESYSTEM') . '/public/archives/archives_' . date('d-m-Y') . '.zip');
        };

        $zipFile = 'archives_' . date('d-m-Y') . '.zip';
        if ($zip->open(env('FILESYSTEM') . '/public/archives/' . $zipFile, \ZipArchive::CREATE)== TRUE) {
            foreach ($files as $key => $f){
                if($f->pdf != "") {
                    $path = env('FILESYSTEM') . 'public/archives/' . $f->pdf;
                    $zip->addFile($path, $f->pdf);
                }
                
                if($f->xml != "") {
                    $path = env('FILESYSTEM') . 'public/archives/' . $f->xml;
                    $zip->addFile($path, $f->xml);
                }
            }
        }

        if($files->count() > 0) {
            return response()->json(['status' => true, 'file' => url('/archives/' . $zipFile)]);
        } else {
            return response()->json(['status' => true, 'message' => 'No se encontraron facturas para esos días']);
        }
    }
}
