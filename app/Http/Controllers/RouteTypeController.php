<?php

namespace App\Http\Controllers;

use App\Models\RouteType;
use Illuminate\Http\Request;

class RouteTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routeTypes = RouteType::all();

        return view('pages.route-type.list', compact('routeTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('pages.route-type.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $exist = RouteType::where('name','=', $request->name)->first();

        if($exist instanceof RouteType) {
            return response()->json(['status' => true, 'redirect' => url('/tipos-de-rutas'), 'message' => 'Esta ruta ya ha sido agregada.']);
        }

        $routeType = new RouteType;
        $routeType->name = $request->name;
        if($routeType->save()) {
            return response()->json(['status' => true, 'redirect' => url('/tipos-de-rutas'), 'message' => 'La ruta ha sido agregada correctamente.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
