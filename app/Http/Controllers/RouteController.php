<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Route;
use App\Models\Vehicles;
use App\Models\Clients;
use App\Models\RouteDetail;
use App\Models\RouteStatus;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function index() {
    	$routes = Route::get();

    	return view('pages.routes.routes-list', compact('routes'));
    }

	public function create() {
		$drivers = User::where('role','=','driver')->get();
		$vehicles = Vehicles::get();
		$clients = Clients::get();
        $routeStatus = RouteStatus::get();

		return view('pages.routes.routes-form', compact('drivers', 'vehicles', 'clients', 'routeStatus'));
    }    

    public function edit(Request $request) {
    	$route = Route::find($request->id);
    	$drivers = User::where('role','=','driver')->get();
		$vehicles = Vehicles::get();
		$clients = Clients::get();
        $routeStatus = RouteStatus::get();

    	if($route instanceof Route ) {
    		return view('pages.routes.routes-form', compact('route', 'drivers', 'vehicles', 'clients', 'routeStatus'));
    	} 

    	return redirect('/rutas');
    }

    public function show(Request $request) {
    	$route = Route::find($request->id);

    	if($route instanceof Route) {
    		return view('pages.routes.routes-record', compact('route'));
    	} 

    	return redirect('/rutas');
    }

    public function store(Request $request) {
    	$route = new Route;
    	$route->id_driver = $request->id_driver;
    	$route->id_vehicle = $request->id_vehicle;
    	$route->date = $request->date;
    	$route->id_route_status = $request->id_route_status;

    	try {
    		if($route->save()) {
                foreach (json_decode($request->routes) as $r) {
                    $rd = new RouteDetail;
                    $rd->id_route = $route->id;
                    $rd->id_client = $r->id_client; 
                    $rd->id_location = $r->id_location;
                    $rd->packages = $r->packages;
                    $rd->id_route_status = $r->id_route_status;
                    $rd->save();
                }

    			echo json_encode(array('status' => true, 'message' => 'La ruta ha sido programada correctamente.', 'redirect' => url('/rutas/' . $route->id)));
    			exit;
    		}	
    	} catch (Exception $e) {
    		echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    		exit;
    	}
    }

    public function update(Request $request) {
    	$route = Route::find($request->id);

    	if($route instanceof Route) {
    		$route->id_driver = $request->id_driver;
	    	$route->id_vehicle = $request->id_vehicle;
	    	$route->date = $request->date;
            $route->id_route_status = $request->id_route_status;

    		try {
    			if($route->save()) {

                    foreach (json_decode($request->routes) as $r) {
                        
                    }
    				echo json_encode(array('status' => true, 'message' => 'La ruta ha sido editada correctamente.', 'redirect' => url('/rutas/' . $route->id)));
    				exit;
    			}	
    		} catch (Exception $e) {
    			echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    			exit;
    		}
    	}
    }
}
