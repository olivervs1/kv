<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index() {
    	$clients = Clients::get();
    	return view('pages.clients.clients-list',compact('clients'));
    }

	public function create() {
		return view('pages.clients.clients-form');
    }    

    public function edit(Request $request) {
    	$client = Clients::find($request->id);

    	if($client instanceof Clients ) {
    		return view('pages.clients.clients-form', compact('client'));
    	} 

    	return redirect('/clientes');
    }

    public function show(Request $request) {
    	$client = Clients::find($request->id);

    	if($client instanceof Clients ) {
    		return view('pages.clients.clients-record', compact('client'));
    	} 

    	return redirect('/clientes');
    }

    public function store(Request $request) {
    	$client = new Clients;
    	$client->company_name = $request->company_name;
    	$client->comercial_name = $request->comercial_name;
    	$client->rfc = $request->rfc;
    	$client->contact_name = $request->contact_name;
    	$client->contact_phone = $request->contact_phone;
    	$client->contact_email = $request->contact_email;

    	try {
    		if($client->save()) {
    			echo json_encode(array('status' => true, 'message' => 'El usuario ha sido agregado correctamente.', 'redirect' => url('/clientes/' . $client->id)));
    			exit;
    		}	
    	} catch (Exception $e) {
    		echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    		exit;
    	}
    }

    public function update(Request $request) {
    	$client = Clients::find($request->id);

    	if($client instanceof Clients) {
    		$client->company_name = $request->company_name;
    		$client->comercial_name = $request->comercial_name;
    		$client->rfc = $request->rfc;
    		$client->contact_name = $request->contact_name;
    		$client->contact_phone = $request->contact_phone;
    		$client->contact_email = $request->contact_email;

    		try {
    			if($client->save()) {
    				echo json_encode(array('status' => true, 'message' => 'El usuario ha sido editado correctamente.', 'redirect' => url('/clientes/' . $client->id)));
    				exit;
    			}	
    		} catch (Exception $e) {
    			echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    			exit;
    		}
    	}
    }

    public function getLocations(Request $request) {
        $client = Clients::find($request->id);

        if($client instanceof Clients) {
            $data = array();

            foreach ($client->Locations as $l) {
                $name = $l->name;
                if($l->store_id != "")  {
                    $name = $l->store_id . ' - ' . $l->name;
                }
                $data[] = array('id' => $l->id, 'name' => $l->store_id . ' - ' . $name);
            }
            echo json_encode(array('status' => true, 'data' => $data));
            exit;
        }

        echo json_encode(array('status' => false));
        exit;
    }
}
