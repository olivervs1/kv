<?php

namespace App\Http\Controllers;

use App\Models\CancelMotives;
use Illuminate\Http\Request;

class CancelMotivesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $motives = CancelMotives::get();
        return view('pages.cancel-motives.cancel-motives-list',compact('motives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('pages.cancel-motives.cancel-motives-form');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $motive = new CancelMotives;
        $motive->motive = $request->motive;
        $motive->status = $request->status;

        if($motive->save()) {
            return response()->json(array('status' => true, 'redirect' => url('/motivos-de-cancelacion')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $motive = CancelMotives::find($id);

        if($motive instanceof CancelMotives) {
            return view('pages.cancel-motives.cancel-motives-form', compact('motive'));   
        }
        return redirect(url('/motivos-de-cancelacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $motive = CancelMotives::find($id);
        
        if($motive instanceof CancelMotives) {
            $motive->motive = $request->motive;
            $motive->status = $request->status;

            if($motive->save()) {
                echo json_encode(array('status' => true, 'redirect' => url('/motivos-de-cancelacion')));
                exit;
            }
        }

        return redirect(url('/motivos-de-cancelacion'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
