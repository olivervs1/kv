<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Drivers;
use App\Models\Message;
use App\Models\Vehicles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DriversController extends Controller
{
    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
    	$drivers = User::where('role','=', 'driver')->get();
    	return view('pages.drivers.drivers-list',compact('drivers'));
    }

	public function create() {
        $vehicles = Vehicles::get();
		return view('pages.drivers.drivers-form', compact('vehicles'));
    }    

    public function edit(Request $request) {
    	$driver = User::find($request->id);
        $vehicles = Vehicles::get();

    	if($driver instanceof User ) {
    		return view('pages.drivers.drivers-form', compact('driver', 'vehicles'));
    	} 

    	return redirect('/choferes');
    }

    public function show(Request $request) {
    	$driver = User::find($request->id);

    	if($driver instanceof User ) {
    		return view('pages.drivers.drivers-record', compact('driver'));
    	} 

    	return redirect('/choferes');
    }

    public function store(Request $request) {
    	$driver = new User;
    	$driver->name = $request->name;
    	$driver->last_name_father = $request->last_name_father;
    	$driver->last_name_mother = $request->last_name_mother;
    	$driver->phone = $request->phone;
    	$driver->email = $request->email;
    	$driver->employee_number = $request->employee_number;
        $driver->id_vehicle = $request->id_vehicle;
        $driver->role = "driver";
        $driver->password = Hash::make("pruebas");

    	try {
    		if($driver->save()) {
    			echo json_encode(array('status' => true, 'message' => 'El chofer ha sido agregado correctamente.', 'redirect' => url('/choferes/' . $driver->id)));
    			exit;
    		}	
    	} catch (Exception $e) {
    		echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    		exit;
    	}
    }

    public function update(Request $request) {
    	$driver = User::find($request->id);

    	if($driver instanceof User) {
    		$driver->name = $request->name;
	    	$driver->last_name_father = $request->last_name_father;
	    	$driver->last_name_mother = $request->last_name_mother;
	    	$driver->phone = $request->phone;
	    	$driver->email = $request->email;
	    	$driver->employee_number = $request->employee_number;
            $driver->id_vehicle = $request->id_vehicle;
            
    		try {
    			if($driver->save()) {
    				echo json_encode(array('status' => true, 'message' => 'El chofer ha sido editado correctamente.', 'redirect' => url('/choferes/' . $driver->id)));
    				exit;
    			}	
    		} catch (Exception $e) {
    			echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    			exit;
    		}
    	}
    }

    public function test() {
        $drivers = Drivers::get();

        foreach($drivers as $d) {
            $user = new User;
            $user->name = $d->name;
            $user->last_name_father = $d->last_name_father;
            $user->last_name_father = $d->last_name_mother;
            $user->phone = $d->phone;
            $user->email = $d->email;
            $user->employee_number = $d->employee_number;
            $user->role = "driver";
            $user->id_vechicle = $d->vehicle;
            $user->password = "$2y$10$2du767ht5X7Y.6XALzAwIu.8FnIUIjubSI1C5qDQtO8hn9EnbJpuW";
            $user->save();
        }

        echo 'terminado';
    }

    public function Chat($id) {
        $driver = User::find($id);

        $messages = Message::where('receiver_id', '=', $driver->id)->orWhere('sender_id', '=', $driver->id)->orderBy('id', 'asc')->limit('1000')->get();

        return view('pages.drivers.drivers-chat', compact('messages', 'driver'));
    }

    public function sendMessage($id, Request $request) {
        $driver = User::find($id);
        $admin = Auth::user();

        $msg = $request->message;

        $message = new Message;
        $message->sender_id = $admin->id; 
        $message->receiver_id = $driver->id;
        $message->message = $msg;
        if($message->save()) {
            return response()->json(['status' => true]);
        }
    }
}
