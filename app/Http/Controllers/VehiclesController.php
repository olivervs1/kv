<?php

namespace App\Http\Controllers;

use App\Models\Vehicles;
use Illuminate\Http\Request;

class VehiclesController extends Controller
{
public function index() {
    	$vehicles = Vehicles::get();
    	return view('pages.vehicles.vehicles-list',compact('vehicles'));
    }

	public function create() {
		return view('pages.vehicles.vehicles-form');
    }    

    public function edit(Request $request) {
    	$vehicle = Vehicles::find($request->id);

    	if($vehicle instanceof Vehicles ) {
    		return view('pages.vehicles.vehicles-form', compact('vehicle'));
    	} 

    	return redirect('/choferes');
    }

    public function show(Request $request) {
    	$vehicle = Vehicles::find($request->id);

    	if($vehicle instanceof Vehicles ) {
    		return view('pages.vehicles.vehicles-record', compact('vehicle'));
    	} 

    	return redirect('/choferes');
    }

    public function store(Request $request) {
    	$vehicle = new Vehicles;
    	$vehicle->name = $request->name;
    	$vehicle->mark = $request->mark;
    	$vehicle->model = $request->model;
    	$vehicle->year = $request->year;
    	$vehicle->plates = $request->plates;

    	try {
    		if($vehicle->save()) {
    			echo json_encode(array('status' => true, 'message' => 'El vehículo ha sido agregado correctamente.', 'redirect' => url('/vehiculos/' . $vehicle->id)));
    			exit;
    		}	
    	} catch (Exception $e) {
    		echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    		exit;
    	}
    }

    public function update(Request $request) {
    	$vehicle = Vehicles::find($request->id);

    	if($vehicle instanceof Vehicles) {
    		$vehicle->name = $request->name;
	    	$vehicle->mark = $request->mark;
	    	$vehicle->model = $request->model;
	    	$vehicle->year = $request->year;
	    	$vehicle->plates = $request->plates;

    		try {
    			if($vehicle->save()) {
    				echo json_encode(array('status' => true, 'message' => 'El vehículo ha sido editado correctamente.', 'redirect' => url('/vehiculos/' . $vehicle->id)));
    				exit;
    			}	
    		} catch (Exception $e) {
    			echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    			exit;
    		}
    	}
    }
}
