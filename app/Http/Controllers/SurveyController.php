<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $questions = Survey::get();

        return view('pages.survey.survey-list',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('pages.survey.survey-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $question = $request->question;
        $status = $request->status;

        $q = new Survey;
        $q->question = $question;
        $q->status = $status;
        if($q->save()) {
            echo json_encode(array('status' => true, 'message' => 'La pregunta ha sido agregada correctamente.', 'redirect' => url('/encuesta/')));
            exit;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $question = Survey::find($id);

        if($question instanceof Survey) {
            return view('pages.survey.survey-form', compact('question'));
        }

        echo json_encode(array('status' => true, 'message' => 'La pregunta ha sido editada correctamente.', 'redirect' => url('/encuesta/')));
        exit;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $q = Survey::find($id);

        if($q instanceof Survey) {
            $question = $request->question;
            $status = $request->status;

            $q->question = $question;
            $q->status = $status;
            if($q->save()) {
                return redirect(url('/encuesta/' . $q->id . '/editar'));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
