<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\PreconfiguredRoute;
use App\Models\PreconfiguredRouteLocation;
use App\Models\RouteType;
use Illuminate\Http\Request;

class PreconfiguredRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $routes = PreconfiguredRoute::get();

        return view('pages.preconfigured-routes.list', compact('routes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $route_types = RouteType::all();
        $clients = Clients::orderBy('comercial_name')->get();

        return view('pages.preconfigured-routes.form', compact('route_types', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $preconfiguredRoute = new PreconfiguredRoute;
        $preconfiguredRoute->name = $request->name;
        $preconfiguredRoute->route_type_id = $request->route_type_id;
        if($preconfiguredRoute->save()) {
            foreach ($request->id_client as $k => $v) {
                $route = new PreconfiguredRouteLocation;
                $route->preconfigured_route_id = $preconfiguredRoute->id;
                $route->client_id = $request->id_client[$k];
                $route->location_id = $request->id_location[$k];
                $route->save();
            }
        }

        return response()->json(['status' => true, 'message' => 'Las ruta preconfigurada ha sido guardada exitosamente.', 
            'redirect' => url('/rutas-preconfiguradas')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $route = PreconfiguredRoute::find($id);
        $route_types = RouteType::all();
        $clients = Clients::orderBy('comercial_name')->get();

        return view('pages.preconfigured-routes.form', compact('route_types', 'clients', 'route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $preconfiguredRoute = PreconfiguredRoute::find($id);
        $preconfiguredRoute->name = $request->name;
        $preconfiguredRoute->route_type_id = $request->route_type_id;
        if($preconfiguredRoute->save()) {
            foreach ($preconfiguredRoute->Locations as $loc) {
                $loc->delete();
            }

            foreach ($request->id_client as $k => $v) {
                $route = new PreconfiguredRouteLocation;
                $route->preconfigured_route_id = $preconfiguredRoute->id;
                $route->client_id = $request->id_client[$k];
                $route->location_id = $request->id_location[$k];
                $route->save();
            }
        }

        return response()->json(['status' => true, 'message' => 'Las ruta preconfigurada ha sido editada exitosamente.', 
            'redirect' => url('/rutas-preconfiguradas')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
