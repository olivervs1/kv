<?php

namespace App\Http\Controllers;

use App\Models\Route;
use App\Models\Config;
use Illuminate\Http\Request;

use Mailgun\Mailgun;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $date = $request->date != NULL ? $request->date : date('Y-m-d');
        $routes = Route::where('date', '=', $date)->get();

        return view('home', compact('routes', 'date'));
    }

    public function test() {
        $ch = curl_init("https://ifconfig.me");
        $fp = fopen("my_ip.txt", "w");

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);


        $ip = file_get_contents('my_ip.txt');
        
        $config = Config::first();

        if($config == NULL) {
            $config = new Config;
            $config->name = 'ip';
            $config->value = $ip;
            $config->save();
        } else {
            if($ip == $config->value && 1==2) {
                echo "todo igual";
                return;
            } else {
                echo "cambio";
                $config->value = $ip;
                $config->save();

                $data = array();
                $data['ip'] = $config->value;

                Mail::send('emails.ip_change', $data, function($message) use ($config) {
                    $message->from('kv@logistics.com', 'KV Transportes');
                    //$message->to('yazmin.beristain@gameplanet.com');
                    //$message->cc(['olivervs@hotmail.es', 'ajimenezkv@gmail.com', 'kvtransportes01@gmail.com']);
                    $message->to('olivervs@hotmail.es');
                    $message->subject('¡Cambio de IP!');
                });

                return;
            }
        }

        var_dump($config); exit;
    }
}
