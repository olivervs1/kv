<?php
namespace App\Http\Controllers;

use DB;
use JWTAuth;
use App\Models\User;
use App\Models\Message;
use App\Models\RouteDetail;
use App\Models\RouteTrace;
use App\Models\CancelMotives;
use App\Models\Survey;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    public function register(Request $request)
    {
    	//Validate data
        $data = $request->only('name', 'email', 'password');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, create new user
        $user = User::create([
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => bcrypt($request->password)
        ]);

        //User created, return success response
        return response()->json([
            'success' => true,
            'message' => 'User created successfully',
            'data' => $user
        ], Response::HTTP_OK);
    }
 
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }
        
        //Request is validated
        //Crean token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }
 	
 		//Token created, return with success response and jwt token
        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }
 
    public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

		//Request is validated, do logout        
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User has been logged out'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
 
    public function get_user(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
        return response()->json(['user' => $user]);
    }

    /***
     * Método para obtener las rutas Pendientes de un transportista
     * 
     * 
     * 
    ***/
    public function getRoutes(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
        if($user) {
            $routes = $user->Routes;
            $routesArr = array();

            if(count($routes) > 0) {
                foreach($routes as $k => $r) {

                    if(isset($request->date)) {
                        if($r->date == $request->date) {
                            foreach($r->Detail as $rd) {
                                $routesArr[] = array(
                                    'id_route_detail'   => $rd->id,
                                    'id_client'         => $rd->id_client,
                                    'id_location'       => $rd->id_location,
                                    'location_name'     => $rd->Client->comercial_name . ' - ' . $rd->Location->name,
                                    'packages'          => $rd->packages,
                                    'id_status'         => $rd->id_route_status,
                                    'status'            => $rd->Status->name,
                                    'address'           => [
                                        'street'        => $rd->Location->street,
                                        'ext_number'    => $rd->Location->ext_number,
                                        'int_number'    => $rd->Location->int_number,
                                        'colony'        => $rd->Location->colony,
                                        'city'          => $rd->Location->city,
                                        'state'         => $rd->Location->state,
                                        'zipcode'       => $rd->Location->zipcode,
                                        'lat'           => $rd->Location->lat == "" ? NULL : floatval($rd->Location->lat),
                                        'long'          => $rd->Location->long == "" ? NULL : floatval($rd->Location->long)
                                    ]
                                );
                            }
                        }
                    } else {
                        foreach($r->Detail as $rd) {
                            $routesArr[] = array(
                                'id_route_detail'   => $rd->id,
                                'id_client'         => $rd->id_client,
                                'id_location'       => $rd->id_location,
                                'location_name'     => $rd->Client->comercial_name . ' - ' . $rd->Location->name,
                                'packages'          => $rd->packages,
                                'id_status'         => $rd->id_route_status,
                                'status'            => $rd->Status->name,
                                'address'           => [
                                    'street'        => $rd->Location->street,
                                    'ext_number'    => $rd->Location->ext_number,
                                    'int_number'    => $rd->Location->int_number,
                                    'colony'        => $rd->Location->colony,
                                    'city'          => $rd->Location->city,
                                    'state'         => $rd->Location->state,
                                    'zipcode'       => $rd->Location->zipcode,
                                    'lat'           => $rd->Location->lat == "" ? NULL : floatval($rd->Location->lat),
                                    'long'          => $rd->Location->long == "" ? NULL : floatval($rd->Location->long)
                                ]
                            );
                        }
                    }
                }
            }

            return response()->json(['status' => true, 'routes' => $routesArr]);    
        } 

        return response()->json(['status' => false, 'message' => 'El Token no es válido.']);
    }

    public function updateRoute(Request $request) {
        //$token = "";
        $id = $request->id; // ID DE RUTA (id_route_detail)
        $lat = $request->lat;
        $long = $request->lng;
        $date = $request->date;

        //$user = JWTAuth::authenticate($request->token);
        
        $rd = RouteDetail::find($id);

        if(!$rd instanceof RouteDetail) {
            return response()->json(['status' => false, 'message' => 'El ID de la ruta es inválido.']);
        }

        if($rd->start == NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta no ha sido marcada como iniciada por lo tanto no puede actualizar su posición.']);   
        }

        if($rd->end != NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta ya ha sido marcada como finalizada por lo tanto no puede actualizar su posición.']);   
        }

        $rt = new RouteTrace;       
        $rt->id_route_detail = $id;
        $rt->lat = $lat;
        $rt->long = $long;
        $rt->date = $date;
        if($rt->save()) {
            return response()->json(['status' => true, 'message' => 'Se ha recibido la información correctamente.']);
        }

        return response()->json(['status' => false, 'message' => 'Ha habido algún error por favor revisa los parámetros correctamnente.']);
    }

    public function cancelRoute(Request $request) {
        //$token = "";
        $id = $request->id; // ID DE RUTA (id_route_detail)
        $lat = $request->lat;
        $long = $request->lng;
        $date = $request->date;
        $id_cancel_motive = $request->motiveId;

        //$user = JWTAuth::authenticate($request->token);
        
        $rd = RouteDetail::find($id);

        if(!$rd instanceof RouteDetail) {
            return response()->json(['status' => false, 'message' => 'El ID de la ruta es inválido.']);
        }

        if($rd->start == NULL) {
            $rd->start = $date;
        }

        if($rd->end != NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta ya ha sido marcada como finalizada por lo tanto no se puede cancelar.']);   
        }

        $rd->end = $date;
        $rd->id_route_status = 4;
        $rd->id_cancel_motive = $id_cancel_motive;
        $rd->save();

        $rt = new RouteTrace;       
        $rt->id_route_detail = $id;
        $rt->lat = $lat;
        $rt->long = $long;
        $rt->date = $date;
        if($rt->save()) {
            return response()->json(['status' => true, 'message' => 'Se ha recibido la información correctamente.']);
        }

        return response()->json(['status' => false, 'message' => 'Ha habido algún error por favor revisa los parámetros correctamnente.']);
    }

    /***
     * @params $token = JWTToken
     * @params $id = ID de la ruta
     * @params $lat = Latitud
     * @params $long = Longitud
     * @params $date = (YYYY-mm-dd HH:ii:ss) fecha y hora de inicio del viaje
     * 
     * Con este método se marca el inicio de la ruta (Chofer a un destino)
     * 
     ***/
    public function startRoute(Request $request) {
        //$token = "";
        $id = $request->id; // ID DE RUTA (id_route_detail)
        $lat = $request->lat;
        $long = $request->lng;
        $date = $request->date;

        //$user = JWTAuth::authenticate($request->token);
        
        $rd = RouteDetail::find($id);

        /** @TODO: Validar que ID Ruta pertenezca al USUARIO **/

        if(!$rd instanceof RouteDetail) {
            return response()->json(['status' => false, 'message' => 'El ID de la ruta es inválido.']);
        }

        if($rd->start != NULL || $rd->end != NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta ya ha sido marcada como iniciada o ya esta finalizada.']);   
        }

        $rd->start = $date;
        $rd->id_route_status = 2;
        if($rd->save()) {
            $rt = new RouteTrace;       
            $rt->id_route_detail = $id;
            $rt->lat = $lat;
            $rt->long = $long;
            $rt->date = $date;
            if($rt->save()) {
                return response()->json(['status' => true, 'message' => 'Se ha recibido la información correctamente y el viaje ha sido iniciado.']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'El parámetro :start es incorrecto, asegurate de enviarlo de la siguiente manera: YYYY-mm-dd HH:ii:ss.']);    
        }

        return response()->json(['status' => false, 'message' => 'Ha habido algún error por favor revisa los parámetros correctamnente.']);
    }

    /***
     * @params $token = JWTToken
     * @params $id = ID de la ruta
     * @params $lat = Latitud
     * @params $long = Longitud
     * @params $date = (YYYY-mm-dd HH:ii:ss) fecha y hora de finalización del viaje
     * 
     * Con este método se marca el final de la ruta (Chofer a un destino)
     * 
     ***/
    public function endRoute(Request $request) {
        //$token = "";
        $id = $request->id; // ID DE RUTA (id_route_detail)
        $lat = $request->lat;
        $long = $request->lng;
        $date = $request->date;

        //$user = JWTAuth::authenticate($request->token);
        
        $rd = RouteDetail::find($id);


        /** @TODO: Validar que ID Ruta pertenezca al USUARIO **/

        if(!$rd instanceof RouteDetail) {
            return response()->json(['status' => false, 'message' => 'El ID de la ruta es inválido.']);
        }

        if($rd->start == NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta aún no ha sido marcada como iniciada.']);   
        }

        if($rd->end != NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta ya ha sido marcada como finalizada.']);   
        }

        $rd->end = $date;
        $rd->id_route_status = 3;
        if($rd->save()) {
            $rt = new RouteTrace;       
            $rt->id_route_detail = $id;
            $rt->lat = $lat;
            $rt->long = $long;
            $rt->date = $date;
            if($rt->save()) {
                return response()->json(['status' => true, 'message' => 'Se ha recibido la información correctamente y el viaje ha sido finalizado.']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'El parámetro :start es incorrecto, asegurate de enviarlo de la siguiente manera: YYYY-mm-dd HH:ii:ss.']);    
        }

        return response()->json(['status' => false, 'message' => 'Ha habido algún error por favor revisa los parámetros correctamnente.']);
    }

    /***
     * @params $token = JWTToken
     * @params $id = ID de la ruta
     * @params $pic = URL con foto de evidencia de entrega de mercancía
     * @params $sign = URL con foto de la firma de quien recibe
     * @params $name = Nombre de la persona quien recibe la mercancía
     * @params $survey = JSON con las respuestas de la encuesta
     * 
     * Con este método se marca el final de la ruta (Chofer a un destino)
     * 
     ***/
    public function finishRoute(Request $request) {
        //$token = "";
        $id = $request->id; // ID DE RUTA (id_route_detail)
        $sign = $request->sign;
        $pic = $request->pic;
        $name = $request->name;
        $survey = $request->survey;

        //$user = JWTAuth::authenticate($request->token);
        
        $rd = RouteDetail::find($id);


        /** @TODO: Validar que ID Ruta pertenezca al USUARIO **/

        if(!$rd instanceof RouteDetail) {
            return response()->json(['status' => false, 'message' => 'El ID de la ruta es inválido.']);
        }

        if($rd->start == NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta no ha sido marcada como iniciada.']);   
        }

        if($rd->end == NULL) {
            return response()->json(['status' => false, 'message' => 'La ruta no ha sido marcada como finalizada.']);   
        }

        $rd->receiver = $request->name;
        $rd->evidence = $request->pic;
        $rd->sign = $request->sign;
        $rd->survey = $request->survey;
        $rd->id_route_status = 3;
        if($rd->save()) {
            return response()->json(['status' => true, 'message' => 'La ruta ha sido finalizada con éxito.']);
        }
    }

    /***
     * 
     * Con este método se envía el formulario que habrá de llenar el cliente al momento de recibir la mercancía
     * 
     ***/
    public function getSurvey() {
        $questions = Survey::where('status','=', 1)->get();
        $questionsArr = array();

        foreach($questions as $q) {
            $questionsArr[] = array('id' => $q->id, 'question' => $q->question);
        }
    
        return response()->json(['status' => true, 'survey' => $questionsArr]);    
    }

    /***
     * 
     * Con este método se envían las distintas opciones activas para poder cancelar una entrega
     * 
     ***/
    public function getCancelMotives() {
        $motives = CancelMotives::where('status','=', 1)->get();
        $motivesArr = array();

        foreach($motives as $m) {
            $motivesArr[] = array('id' => $m->id, 'motive' => $m->motive);
        }
    
        return response()->json(['status' => true, 'motives' => $motivesArr]);    
    }

    public function cleanData() {
        $routes = RouteDetail::get();

        foreach ($routes as $r) {
            $r->start = null;
            $r->end = null;
            $r->receiver = null;
            $r->evidence = null;
            $r->sign = null;
            $r->id_route_status = 1;
            $r->survey = null;
            if($r->save()) {
                foreach ($r->Trace as $t) {
                    $t->delete();
                }
            }
        }
    }

    public function getRouteDetail(Request $request) {
        $rd = RouteDetail::find($request->id);

        if($rd instanceof RouteDetail) {
            $arr = array();

            foreach($rd->Trace as $t) {
                $arr[] = array(
                    'lat' => $t->lat,
                    'lng' => $t->long,
                    'date' => $t->date
                );
            }

            return response()->json(['status' => true, 'detail' => $arr]);
        }
    }

    /*{
        "author": {
          "firstName": "Alex",
          "id": "b4878b96-efbc-479a-8291-474ef323dec7",
          "imageUrl": "https://avatars.githubusercontent.com/u/14123304?v=4"
        },
        "createdAt": 1598438795000,
        "id": "e7a673e9-86eb-4572-936f-2882b0183cda",
        "status": "seen",
        "text": "Dolore labore ipsum aspernatur. Omnis minima reprehenderit perspiciatis sunt rerum facilis consequatur omnis. Rerum totam est eos dolores quia qui aut. Rerum aperiam alias placeat odit non enim corporis unde molestiae. Aspernatur unde praesentium eum ut laudantium ea enim.",
        "type": "text"
    }*/
    public function sendMessage(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);
        
        $user = JWTAuth::authenticate($request->token);

        $author = $user->id;
        $receiver_id = $request->receiver_id;
        $firstname = $request->message['author']['firstName'];
        $created_at = $request->message['createdAt'];
        $msg = $request->message['text'];
        $msgId = $request->message['id'];

        $message = new Message;
        $message->sender_id = $author; 
        $message->receiver_id = $receiver_id;
        $message->message = $msg;
        if($message->save()) {
            $messages = Message::where(function($query) use ($user, $receiver_id) {
                            $query->where('sender_id','=', $user->id);
                            $query->where('receiver_id','=', $receiver_id);
                        })->orWhere(function($query) use ($user, $receiver_id) {
                            $query->where('sender_id','=', $receiver_id);
                            $query->where('receiver_id','=', $user->id);
                        })->get();

            $msgArr = array();
            foreach ($messages as $k => $m) {
                $msgArr[$k] = array();
                $msgArr[$k]['author']['firstname'] = $m->Sender->name;
                $msgArr[$k]['author']['id'] = $m->sender_id;
                $msgArr[$k]['author']['imageUrl'] = "";
                $msgArr[$k]['created_at'] = strtotime($m->created_at);
                $msgArr[$k]['id'] = strval($m->id);
                $msgArr[$k]['status'] = "";
                $msgArr[$k]['text'] = $m->message;
                $msgArr[$k]['type'] = 'text';
            }

            return response()->json(['status' => true, 'messages' => $msgArr]);
        } 
    }

    public function getMessages(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);

        $receiver_id = $request->receiver_id;
        
        $user = JWTAuth::authenticate($request->token);

        $messages = Message::where(function($query) use ($user, $receiver_id) {
                    $query->where('sender_id','=', $user->id);
                    $query->where('receiver_id','=', $receiver_id);
                })->orWhere(function($query) use ($user, $receiver_id) {
                    $query->where('sender_id','=', $receiver_id);
                    $query->where('receiver_id','=', $user->id);
                })->get();

        $msgArr = array();

        foreach ($messages as $k => $m) {
            $msgArr[$k] = array();
            $msgArr[$k]['author']['firstname'] = $m->Sender->rname;
            $msgArr[$k]['author']['id'] = $m->sender_id;
            $msgArr[$k]['author']['imageUrl'] = "";
            $msgArr[$k]['created_at'] = strtotime($m->created_at);
            $msgArr[$k]['id'] = strval($m->id);
            $msgArr[$k]['status'] = "";
            $msgArr[$k]['text'] = $m->message;
            $msgArr[$k]['type'] = 'text';
        }

        return response()->json(['status' => true, 'messages' => $msgArr]);
    }

    public function getChatUsers()  {
        $users = User::where('role','=', 'admin')->get();

        $usersArr = array();

        foreach ($users as $u) {
            $usersArr[] = array('id' => $u->id, 'name' => $u->name . ' ' . $u->lastname, 'avatar' => 'http://kv.1808.studio/public/img/avatar.png');
        }

        return response()->json(['status' => true, 'users' => $usersArr]);
    }
}
