<?php

namespace App\Http\Controllers;

use App\Models\RouteStatus;
use Illuminate\Http\Request;

class RouteStatusController extends Controller
{
    public function index() {
    	$routesStatus = RouteStatus::get();
        
    	return view('pages.route-status.route-status-list', compact('routesStatus'));
    }

	public function create() {
        return view('pages.route-status.route-status-form');
    }    

    public function edit(Request $request) {
    	$routeStatus = RouteStatus::find($request->id);

    	if($routeStatus instanceof RouteStatus) {
    		return view('pages.route-status.route-status-form', compact('routeStatus'));
    	} 

    	return redirect('/rutas');
    }

    public function show(Request $request) {
    	$routeStatus = RouteStatus::find($request->id);

    	if($routeStatus instanceof RouteStatus ) {
    		return view('pages.route-status.route-status-record', compact('routeStatus'));
    	} 

    	return redirect('/rutas');
    }

    public function store(Request $request) {
    	$routeStatus = new RouteStatus;
    	$routeStatus->name = $request->name;
    	$routeStatus->color = $request->color;

		if($routeStatus->save()) {

			echo json_encode(array('status' => true, 'message' => 'Es estatus de ruta ha sido agregado correctamente.', 'redirect' => url('/estatus-rutas/' . $routeStatus->id)));
			exit;
		}	
    }

    public function update(Request $request) {
    	$routeStatus = RouteStatus::find($request->id);

    	if($routeStatus instanceof RouteStatus) {
            $routeStatus->name = $request->name;
            $routeStatus->color = $request->color;

			if($routeStatus->save()) {
				echo json_encode(array('status' => true, 'message' => 'El estatus de ruta ha sido editado correctamente.', 'redirect' => url('/estatus-rutas/' . $routeStatus->id)));
				exit;
			}	
    	}
    }
}
