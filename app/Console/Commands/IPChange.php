<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Config;

use Mailgun\Mailgun;
use Illuminate\Support\Facades\Mail;

class IPChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a mail to Gameplanet and admins if the Server Public IP change';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ch = curl_init("https://ifconfig.me");
        $fp = fopen("my_ip.txt", "w");

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);


        $ip = file_get_contents('my_ip.txt');
        
        $config = Config::first();

        if($config == NULL) {
            $config = new Config;
            $config->name = 'ip';
            $config->value = $ip;
            $config->save();
        } else {
            if($ip == $config->value) {
                echo "todo igual";
                return;
            } else {
                echo "cambio";
                $config->value = $ip;
                $config->save();

                $data = array();
                $data['ip'] = $config->value;

                Mail::send('emails.ip_change', $data, function($message) use ($config) {
                    $message->from('kv@logistics.com', 'KV Transportes');
                    $message->to('yazmin.beristain@gameplanet.com');
                    $message->cc(['olivervs@hotmail.es', 'ajimenezkv@gmail.com', 'kvtransportes01@gmail.com']);
                    $message->subject('¡Cambio de IP!');
                });

                return;
            }
        }        
    }
}
