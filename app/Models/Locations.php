<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    use HasFactory;

    public function Client() {
    	return $this->belongsTo('App\Models\Clients', 'id_client', 'id');
    }
}
