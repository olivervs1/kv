<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    const ADMIN = "admin";
    const DRIVER = "driver";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name_father',
        'last_name_mother',
        'employee_number',
        'phone',
        'email',
        'role',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims() {
        return [];
    }

    public function isAdmin() {
        return $this->role === User::ADMIN;
    }

    public function isDriver() {
        return $this->role === User::DRIVER;
    }

    public function Vehicle() {
        return $this->hasOne('App\Models\Vehicles', 'id', 'id_vehicle');
    }

    public function Routes() {
        return $this->hasMany('App\Models\Route', 'id_driver', 'id');
    }
}
