<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreconfiguredRoute extends Model
{
    use HasFactory;

    protected $table = 'preconfigured_route';

    public function Locations() {
        return $this->hasMany('App\Models\PreconfiguredRouteLocation', 'preconfigured_route_id', 'id');
    }

    public function RouteType() {
        return $this->belongsTo('App\Models\RouteType', 'route_type_id', 'id');
    }
}
