<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    use HasFactory;

    protected $table = 'archive';

    public function Client() {
        return $this->belongsTo('App\Models\Clients', 'client_id', 'id');
    }
}
