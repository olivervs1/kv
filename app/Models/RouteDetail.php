<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RouteDetail extends Model
{
    use HasFactory;

    protected $table = "route_detail";
    
    public function Route() {
    	return $this->belongsTo('App\Models\Route', 'id_route', 'id');
    }

    public function Client() {
    	return $this->belongsTo('App\Models\Clients', 'id_client', 'id');
    }

    public function Location() {
    	return $this->belongsTo('App\Models\Locations', 'id_location', 'id');
    }

    public function Status() {
    	return $this->belongsTo('App\Models\RouteStatus', 'id_route_status', 'id');
    }

    public function Trace() {
        return $this->hasMany('App\Models\RouteTrace', 'id_route_detail', 'id');
    }
}
