<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    use HasFactory;

    public function Vehicle() {
    	return $this->belongsTo('App\Models\Vehicles', 'id_vehicle', 'id');
    }
}
