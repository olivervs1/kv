<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use HasFactory;

    protected $table = "route";
    
    public function Driver() {
    	return $this->belongsTo('App\Models\User', 'id_driver', 'id');
    }

    public function Vehicle() {
    	return $this->belongsTo('App\Models\Vehicles', 'id_vehicle', 'id');
    }

    public function Detail() {
    	return $this->hasMany('App\Models\RouteDetail', 'id_route', 'id');
    }

    public function Status() {
    	return $this->belongsTo('App\Models\RouteStatus', 'id_route_status', 'id');
    }
}
