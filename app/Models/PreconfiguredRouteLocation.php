<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreconfiguredRouteLocation extends Model
{
    use HasFactory;

    protected $table = 'preconfigured_routes_locations';

    public function Client() {
        return $this->belongsTo('App\Models\Clients', 'client_id', 'id');
    }

    public function Location() {
        return $this->belongsTo('App\Models\Locations', 'location_id', 'id');
    }
}
