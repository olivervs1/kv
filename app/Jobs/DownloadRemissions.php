<?php

namespace App\Jobs;

use File;
use ZipArchive;
use App\Models\PreconfiguredRoute;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Mail;

class DownloadRemissions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $remissions = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($remissions) {
        $this->remissions = $remissions;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        ///// CODIGO PARA GAMEPLANET /////
        $endpoint = "https://epod4.gameplanet.com/bridge/logistica/carta_porte/mercancia";
        $client = new \GuzzleHttp\Client();
        //$distribucion = '20211030-211039';
        //$destino = "30";

        $remissions = explode(',', $this->remissions);

        $routes = PreconfiguredRoute::all();
        $files = array();
        
        foreach ($routes as $route) {
            foreach ($route->Locations as $loc) {
                $html = '<table>';
                $html .= '<tr>';
                $html .= '<td>Cantidad</td>';
                $html .= '<td>ID unidad embalaje</td>';
                $html .= utf8_decode('<td>Descripción material carga</td>');
                $html .= '<td>Peso</td>';
                $html .= '<td>ID unidad de peso</td>';
                $html .= '<td>Clave Productos y servicios</td>';
                $html .= '<td>Clave Unidad</td>';
                $html .= '<td>ID Tienda</td>';
                $html .= '<td>ID Ruta</td>';
                $html .= '<td>Ruta</td>';
                $html .= '<td>Zona</td>';
                $html .= '<td>Clave</td>';
                $html .= '<td>Remision</td>';
                $html .= '</tr>';
                foreach ($remissions as $remission) {
                    $destino = 0;
                    if($loc instanceof Locations) {
                        $destino = $loc->store_id;
                    } else {
                        $destino = $loc->Location->store_id;
                    }

                    try {
                        $response = $client->request('GET', $endpoint, ['query' => [
                            'distribucion' => trim($remission),
                            'destino' => $destino
                            ],
                            'auth' => [
                                'gp_externo',
                                'tAsN4eWRSG3ByM'
                            ],
                            'http_errors' => false
                        ]);

                        $statusCode = $response->getStatusCode();
                        
                        if($statusCode != 200) {
                            continue;
                        }
                        
                        $content = $response->getBody();

                        // or when your server returns json
                        $content = json_decode($response->getBody(), true);
                    } catch (RequestException $e) {
                        var_dump($e); exit;
                        // Catch all 4XX errors 
                        
                        // To catch exactly error 400 use 
                        if ($e->hasResponse()){
                            if ($e->getResponse()->getStatusCode() == '400') {
                                    var_dump("Got response 400"); exit;
                            }
                        }

                        // You can check for whatever error status code you need 
        
                    } catch (Exception $e) {
                        var_dump($e); exit;
                    }

                    if(isset($content['success'])) {
                        if(isset($content['list']['status']) && $content['list']['status'] == "success") {
                            if(!is_dir(env('FILESYSTEM') . 'public/exports/')) {
                                mkdir(env('FILESYSTEM') . 'public/exports/');
                            }

                            if(!is_dir(env('FILESYSTEM') . 'public/exports/stores/')) {
                                mkdir(env('FILESYSTEM') . 'public/exports/stores/');
                            }

                            foreach($content['list']['list'] as $p) {
                                $html .= '<tr>';
                                    $html .= '<td>' . $p['cantidad'] . '</td>';
                                    $html .= '<td>' . $p['unidad_embalaje'] . '</td>';
                                    $html .= '<td>' . $p['descripcion'] . '</td>';
                                    $html .= '<td>' . $p['peso'] . '</td>';
                                    $html .= '<td>' . $p['unidad_peso'] . '</td>';
                                    $html .= '<td>' . $p['clave_productos_servicios'] . '</td>';
                                    $html .= '<td>' . $p['clave_unidad'] . '</td>';
                                    $html .= '<td>' . $destino . '</td>';
                                    $html .= '<td>' . $p['id_ruta'] . '</td>';
                                    $html .= '<td>' . $p['ruta'] . '</td>';
                                    $html .= '<td>' . $p['zona'] . '</td>';
                                    $html .= '<td>' . $p['clave'] . '</td>';
                                    $html .= '<td>' . $remission . '</td>';
                                $html .= '</tr>';
                            }
                        }
                    }
                }

                $html .= '</table>';

                $filename = $destino . '.xls';

                file_put_contents(env('FILESYSTEM') . '/public/exports/stores/' . $filename, $html);

                $files[] = env('FILESYSTEM') . '/public/exports/stores/' . $filename;
            }   
        }

        $zip = new \ZipArchive();
        $fileName = 'remisiones.zip';
        if ($zip->open(env('FILESYSTEM') . '/public/exports/remisiones.zip', \ZipArchive::CREATE)== TRUE)
        {
            $files = File::files(env('FILESYSTEM') . '/public/exports/stores/');
            foreach ($files as $key => $value){
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }

        $subscribers = array();
        $subscribers[] = array('email' => 'olivervs@hotmail.es', 'name' => 'Oliver Vilchiz');

        foreach ($subscribers as $subscriber) {
            Mail::send('emails.remissions_ready', ['uri' => url('/exports/remisiones.zip')], function ($m) use($subscriber) {
                $m->from('no-replay@kvtransportes.com', 'KV Transportes');
                $m->to($subscriber['email'], $subscriber['name']);
                $m->subject('El archivo esta listo para ser descargado.');
            });
        }
    }
}
